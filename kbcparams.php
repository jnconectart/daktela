<?php
// parametry importované z konfiguračního JSON řetězce v definici PHP aplikace v KBC

$configFile = $dataDir."config.json";
$config     = json_decode(file_get_contents($configFile), true);

// parametry importované z konfiguračního JSON v KBC
$confParam = $config["parameters"];
$processedInstances = $confParam["processedInstances"]; // pole s údaji, které instance mají být zpracovány - např. ["1" => true, "2" => true, ...]
$accessTokens       = $confParam["accessTokens"];       // pole accessTokenů API Daktely (např. pro stahování "channels" hovorových aktivit u ver. >= 6.18)
$channelsApiGet     = $confParam["channelsApiGet"];     // pole s údaji, na kterých instancích se mají stahovat hovorové kanály (channels) z API endpointu "channels.json"
$incrementalMode    = $confParam["incrementalMode"];    // pole s údaji o inkrementálním režimu zpracování
$jsonParse          = $confParam["jsonParse"];          // pole s parametry parsování JSON řetězců
$diagOutOptions     = $confParam["diagOutOptions"];     // diag. výstup do logu Jobs v KBC - klíče: basicStatusInfo, jsonParseInfo // ZRUŠIT ... , basicIntegrInfo, detailIntegrInfo
$adhocDump          = $confParam["adhocDump"];          // diag. výstup do logu Jobs v KBC - klíče: active, idFieldSrcRec

// parametry inkrementálního režimu
$incrementalOn      = empty($incrementalMode['incrementalOn']) ? false : true;          // vstupní hodnota false se vyhodnotí jako empty :)
$incrActivitiesOnly = empty($incrementalMode['incrActivitiesOnly']) ? false : true;     // vstupní hodnota false se vyhodnotí jako empty :)
$histDays           = $incrementalMode['histDays'];     // datumový rozsah historie pro tvorbu reportu - pole má klíče "start" a "end", kde musí být "start" >= "end"

/* import parametru z JSON řetězce v definici Customer Science PHP v KBC:
    {
      "incrementalMode": {
        "incrementalOn": true,
        "incrActivitiesOnly": true,
        "histDays": {
          "start": 1,
          "end": 0
        }
      },
      "processedInstances": {
        "1": false,
        "2": false,
        "3": true,
        "4": true,
        "5": true,
        "6": true,
        "7": true,
        "8": true,
        "9": false,
        "A": true,
        "B": true,
        "C": true,
        "D": true,
        "E": true
      },
      "accessTokens": {
        "1": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "2": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "3": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "4": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "5": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "6": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "7": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "8": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "9": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "A": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "B": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "C": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "D": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "E": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
      },
      "channelsApiGet": {
        "1": false,
        "2": false,
        "3": false,
        "4": false,
        "5": false,
        "6": true,
        "7": false,
        "8": false,
        "9": false,
        "A": false,
        "B": true,
        "C": true,
        "D": false,
        "E": false
      },
      "jsonParse": {
        "activities.item_parseNestedAttrs": false
      },
      "diagOutOptions": {
        "basicStatusInfo": true,
        "jsonParseInfo": false
      },
      "adhocDump": {
        "active": false,
        "needles": {
          "3": {
            "records": {
              "dumpColName": "idrecord",
              "dumpColVals": [
                "301121251",
                "308360199"
              ]
            }
          },
          "6": {
            "activities": {
              "dumpColName": "time",
              "dumpColVals": [
                "2020-01-17 11:41:28",
                "2020-02-19 08:19:22"
              ]
            }
          }
        }
      }
    }
  -> podrobnosti viz https://developers.keboola.com/extend/custom-science
*/