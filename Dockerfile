FROM composer
COPY . /code/
WORKDIR /code/
RUN composer install

WORKDIR /data/
CMD ["php", "/code/main.php"]