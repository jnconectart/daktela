<?php
// funkce

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function blanc ($var = "") {                                                    // modifikovaná varianta defaultní PHP funkce 'empty'   
    switch (gettype($var)) {
        case "NULL":	return true;		break;
    	case "boolean": return false;		break; 
    	case "integer":	return false;		break; 
    	case "string":	return !strlen($var);	break;                          // např. řetězec "0" se musí interpretovat jako neprázdný (fce 'empty' to nesplňuje)
    	case "array":	return empty($var);	break;
    	case "object":	return empty($var);	break;
    	default:	null;
    }
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function arrVal ($keys, $arr, $returnArrAllowed = false, $noExistVal = NULL) {  // test existence klíče(-ů) $keys pole $arr ($key lze u 1D zadat jako string/int, u n-D zadávat jako pole, např. ["uroven1", "uroven2", ... "urovenN"]);
                                                                                // v případě existence klíče vrátí jeho hodnotu, jinak vrátí určenou zástupnou hodnotu (defaultně NULL)
                                                                                // defaultně vrací jen jednoduché hodnoty, v případě potřeby vracet pole je třeba zadat parametr $returnArrAllowed = true (např. u activities.item.channels)
    if(empty($keys) || !is_array($arr)) {return $noExistVal;}                   // nevalidní hodnoty argumentů
    if (!in_array(gettype($keys), ["array", "string", "integer"])) {return $noExistVal;}    // nevalidní typ $keys
    $keyArr = is_array($keys) ? $keys : [$keys];                                // převede string/int s názvem klíče na 1-D pole (aby šlo u 1-D pole zadávat název požadovaného klíče jako string/int místo 1D-pole)
    $keyDimension = count($keyArr);                                             // N (dimenze pole $keyArr)
    $partialVal = $arr;                                                         // prvek pole $arr i-té úrovně (začínáme celým polem $arr = prvek 0. úrovně)
    for ($i = 0; $keyDimension; $i++) {
        if (!array_key_exists($i, $keyArr)) {return $noExistVal;}               // i-tá dimenze v poli $keyArr už nebyla nalezena  
        if (!array_key_exists($keyArr[$i], $partialVal)) {return $noExistVal;}  // i-tá dimenze v poli $arr už nebyla nalezena                                                                                  
        $partialVal = $partialVal[$keyArr[$i]];                                 // $partialVal = postupné zanořování do nižších úrovní pole $arr
        if ($i == $keyDimension - 1) {                                          // zanoření dospělo do úrovně odpovídající dimenzi pole $keyArr
            return is_array($partialVal) && !$returnArrAllowed ? $noExistVal : $partialVal;           
                                                                                // $noExistVal ... dim($keyArr) < dim($arr) [s výjimkou případů, kdy jsme povolili vrátit pole]; ...
        }                                                                       // $partialVal ... dim($keyArr) = dim($arr) [jednoduchá návratová hodnota]
        if (!is_array($partialVal)) {return $noExistVal;}                       // dim($keyArr) > dim($arr) [v $keyArr bylo zadáno více dimenzí než je dimenze pole $arr]
    }
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                                        // prefixování hodnoty atributu identifikátorem instance + nastavení požadované délky num. řetězců
function setIdLength ($instId = "0", $str = "", $useInstPref = true, $objType = "tab") { 
    global $idFormat, $fakeId;
    $len = $objType=="tab" ? $idFormat["idTab"] : $idFormat["idField"]; // jde o ID položky tabuky / ID form. pole (objType = tab / field)
    switch ($str) {
        case NULL:      return "";                      // vstupní hodnota je NULL nebo prázdný řetězec (obojí se interpretuje jako NULL)
        case $fakeId:   return $fakeId;                 // vstupní hodnota je prázdný řetězec po průchodem fcí emptyToNA, tj. $fakeId (typicky '0')
        default:        $idFormated = !empty($len) ? sprintf('%0'.$len.'s', $str) : $str;
                        switch ($useInstPref) {         // true = prefixovat hodnotu identifikátorem instance a oddělovacím znakem
                            case true:  return sprintf('%0'.$idFormat["instId"].'s', $instId) . $idFormat["sep"] . $idFormated;
                            case false: return $idFormated;    
                        }   
    }
}                                                       // prefixují se jen vyplněné hodnoty (strlen > 0)
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function logInfo ($text, $dumpLevel="basicStatusInfo") {// volitelné diagnostické výstupy do logu
    global $diagOutOptions;
    $dumpKey = array_key_exists($dumpLevel, $diagOutOptions) ? $dumpLevel : "basicStatusInfo";
    echo $diagOutOptions[$dumpKey] ? $text."\n" : "";
}
// ==============================================================================================================================================================================================
function descriptParse ($str, $emptyReplNA = false) {   // separace části poznámky uvnitř definovaných delimiterů (např. názvu skupiny, sady několika parametrů dále separovatelných funkcí subparamParse)
    global $delim, $emptyToNA, $fakeId, $descriptParseIgnoredStrings;                 
                                                        // $emptyReplNA ... volitelný spínač zachovávání hodnot $fakeId + vkládání $fakeId u hodnot prázdných po parsování
                                                        // $descriptParseIgnoredStrings ... pole systémových řetězců pro řízení custom skriptů Daktely, které mají být v BI ignorovány (viz 'variables.php')
    $str = str_replace($descriptParseIgnoredStrings, "", $str ?? "");
    $match = [];                                        // "match array"
    preg_match("/".preg_quote($delim["L"])."(.*)".preg_quote($delim["R"])."/s", $str, $match);
    if ($emptyReplNA && $emptyToNA && empty($match[1])) {return $fakeId;}
    return empty($match[1]) ?  "" : $match[1];          // $match[1] obsahuje podřetězec ohraničený delimitery ($match[0] dtto včetně delimiterů)
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function subparamParse ($str, $subpar) {                // separace hodnoty jednoho konkrétního parametru (např. WB:, LOC:) obsaženého v řetězci uvnitř definovaných delimiterů v poli Poznámka (Description)
    $explArr = explode(" ", $str);                      // příklad použití:  subparamParse(descriptParse($poznamka), "LOC:")  ...  CSR lokalita (např. "Drahonice")
    foreach($explArr as $subparamItem) {
        if (strpos($subparamItem, $subpar) === 0) {     // nutno testovat i shodu datového typu int (===), jinak (pro ==) reaguje i na FALSE => skončí hned u 1. iterace
        return substr($subparamItem, strlen($subpar));
        }
    }
    return "";
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function isValidPhoneNumber ($str) {                    // ověření, zda řetězec může být tel. číslo (true / false)
    global $phoneNumMaxLen;
    return is_numeric($str) && strlen($str) <= $phoneNumMaxLen ? true : false;
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function phoneNumberCanonic ($str) {                    // veřejná tel. čísla omezená na číslice 0-9 (48-57D = 30-39H), bez úvodních nul (ltrim)
    $strConvert = ltrim(preg_replace("/[\\x00-\\x2F\\x3A-\\xFF]/", "", $str ?? ""), "0");   // odebrání úvodních nul a znaků (+, ...)
    if (!isValidPhoneNumber($strConvert )) { return $str; }                                 // zadaný řetězec zřejmě není tel. číslo (není numerický a/nebo je moc dlouhý)
    $foreign =  (substr($str,0,2)=="00" && substr($str,0,5)!="00420") ||
                (substr($str,0,1)=="+"  && substr($str,0,4)!="+420")
                ? true : false;                         // začíná-li původní číslo "00xxx" nebo "+xxx", kde xxx != 420, bere se jako zahraniční
    return $foreign ? $strConvert : (strlen($strConvert) == 9 ? "420" : "") . $strConvert;
}
// původní verze funkce - podpora mezinárodních čísel pouze ve formátu "00xxx", nikoli "+xxx"
/*
function phoneNumberCanonic ($str) {                    // veřejná tel. čísla omezená na číslice 0-9 (48-57D = 30-39H), bez úvodních nul (ltrim)
    if (!isValidPhoneNumber($str)) { return $str; }     // zadaný řetězec zřejmě není tel. číslo (není numerický a/nebo je moc dlouhý)
    $foreign = substr($str,0,2)=="00" && substr($str,0,5)!="00420" ? true : false;  // začíná-li původní číslo "00xxx", kde xxx != 420, bere se jako zahraniční
    $strConvert = ltrim(preg_replace("/[\\x00-\\x2F\\x3A-\\xFF]/", "", $str ?? ""), "0");
    return $foreign ? $strConvert : (strlen($strConvert) == 9 ? "420" : "") . $strConvert;
}
*/
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function removeAccents ($str) {                         // odebrání diakritiky (konverze znaků s diakritikou a jiných UTF-8 znaků na znaky bez diakritiky)
    global $accentsConversion;                          // konverzní pole znaků definované ve 'variables.php'
    return strtr($str, $accentsConversion);
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function containsHtml ($str) {                          // test, zda textový řetězec obsahuje HTML tagy
    return $str != strip_tags($str) ? true : false;     // strip_tags ... standardní PHP fce pro odebrání HTML tagů
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function remStrMultipl ($strIn, $delimiter = " ") {                               // převod multiplicitních podřetězců v řetězci na jeden výskyt podřetězce
    // odebrání duplicitních řetězců, které následují po sobě (oddělené delimiterem) [fce array_unique by odebrala i ty, co nenásledují po sobě]
    $arrIn = explode($delimiter, $strIn);               // vstupní řetězec rozdělěný podle definovaného delimiteru do prvků 1D-pole
    $arrOut = [];
    foreach ($arrIn as $i => $val) {                    // $i = 0,1,2,...
        if ($i < count($arrIn) - 1) {                   // nejde o poslední prvek pole
            if ($val != $arrIn[$i+1]) {                 // hodnota iterovaného prvku != hodnota následujícího prvku vst. pole → ...
                $arrOut[] = $val;                       // ... zařadíme iterovaný prvek do výst. pole
            }
        } else {                                        // jde o poslední prvek pole
            $arrOut[] = $val; 
        }
    }    
    $strOut = implode($delimiter, $arrOut);
    return strlen($strOut)> 99  ? $strIn : $strOut;     // řetězce delší než 99 znaků (zpravidla těla e-mailů) se neupravují
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function trim_all ($str, $what = NULL, $thrownWith = " ", $replacedWith = " | ") {      // odebrání nadbytečných mezer a formátovacích znaků z řetězce
    if ($what === NULL) {
        /*  character   dec     hexa    use
            "\0"         0      \\x00   Null Character
            "\t"         9      \\x09   Tab
            "\n"        10      \\x0A   New line
            "\x0B"      11      \\x0B   Vertical Tab
            "\r"        13      \\x0D   New Line in Mac
            " "         32      \\x20   Space       
                                \\xFFFC  OBJECT REPLACEMENT CHARACTER (U+FFFC)
        */
        $charsToThrow   = "\\x00-\\x09\\x0B-\\x20\\xE2\\xFF";  // all white-spaces and control chars (hexa)
        /*  ^           negace
            \\p{L}      písmena
            \\p{N}      číslice
            \\p{P}      interpunkční znaménka (punctuation marks) - .:?!/\-_ atd.
            \\s         "bílé znaky" (whitespace character) mezery, tabulátory, odřádkování
            \\xE2       znak pro Euro - do řetězce přidáno 07. 12. 2023
        */
        //$charsToThrow   = "^\\p{L}^\\p{N}^\\p{P}^\\s";    // řeší odstranění speciálních unicode znaků (např. \\xFFFC, \\xFFFD, ale jen v nových verzích PHP
        $charsToReplace = "\\x0A";                          // new line
    }
    //$str = iconv("UTF-8", "UTF-8//IGNORE", $str);                             // vypuštění ne-UTF8 znaků (funguje jen jen v nových verzích PHP)
    $str = preg_replace("/[".$charsToThrow . "]+/", $thrownWith,   $str ?? ""); // náhrada prázdných a řídicích znaků mezerou
    $str = preg_replace("/[".$charsToReplace."]+/", $replacedWith, $str ?? ""); // náhrada odřádkování znakem "|" (vyskytují se i vícenásobná odřádkování)
    $str = mb_convert_encoding($str, "UTF-8");                                  // odebere další ne-UTF8 znaky (důležité např. u znaku pro Euro) - přidáno 07. 12. 2023
    $count = 1;                                                                 // počitadlo provedených náhrad dvou po sobě jdoucích oddělovacích řetězců za jeden
    while ($count > 0) {
        $str = str_replace(str_repeat($replacedWith, 2), $replacedWith, $str ?? "", $count);         
    }
    $str = str_replace("￼"  , "", $str ?? "");              // \\xFFFC  OBJECT REPLACEMENT CHARACTER (U+FFFC)
    $str = str_replace("\N" , "", $str ?? "");              // zbylé "\N" způsobují chybu importu CSV do výst. tabulek ("Missing data for not-null field")
    return $str;
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function strLenRestrict ($str) {                            // ořezání velmi dlouhých řetězců, např. hodnoty form. polí (GD dovolí max. 65 535 znaků)
    global $strTrimDefaultLen;
    return strlen($str) <= $strTrimDefaultLen ? $str : substr($str, 0, $strTrimDefaultLen)." ... (zkráceno)";
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function strLenRestrict_htmlThrow_remStrMultipl_trimAll ($str) {                // čtyřkombinace uvedených fcí (pro účely normalizace hodnot parsovyných z JSONů)
    $str = strLenRestrict(remStrMultipl(trim_all($str)));
    $str = containsHtml($str) ? "" : $str;                                      // místo řetězců obsahujících HTML (těla e-mailů apod.) vrátí prázdný řetězec
    return mb_convert_encoding($str, "UTF-8", "UTF-8");                         // odebere další ne-UTF8 znaky (např. xC3, ...)
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function remNonprocessableStrings ($str) {                                      // vyřazení hranatých závorek a jejich obsahu - použití pro nezpracovatelmé znaky v comments.description (Samsung)
    global $instId, $regexSquareBracketsAndContent, $regexAlnumAndSpecCharsOnly;
    $strStripped = strip_tags($str);                                            // vyřazení HTML tagů
    if ($instId == '6') {                                                       // vyřazení hranatých závorek a jejich obsahu - zatím jen na instanci Samsung
        $strRemSquareBracketsAndContent = preg_replace($regexSquareBracketsAndContent, "", $strStripped);   // vyřazení hranatých závorek a jejich obsahu
        $strAlnumAndSpecCharsOnly       = removeAccents($strRemSquareBracketsAndContent);                   // odebrání diakritiky (jinak vede po omezení délky k nevalidním UTF-8 znakům na výstupu)
        $strTrimed                      = trim_all($strAlnumAndSpecCharsOnly);                              // ošetření neimportovatelných znaků, např. trimovaný znak pro Euro na konci řetězce - přidáno 07. 12. 2023
    }
    return strLenRestrict($strTrimed);                                          // omezení délky výstupního řetězce
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*  nahrazeno funkcí "strLenRestrict_htmlThrow_remStrMultipl_trimAll" - strLenRestrict by měl být vnější fcí, jinak připustí i některé řetězce delší než 4000 znaků (např. 4203)
function htmlThrow_remStrMultipl_trimAll_strLenRestrict ($str) {                // čtyřkombinace uvedených fcí (pro účely normalizace hodnot parsovyných z JSONů)
    $strOut = remStrMultipl(trim_all(strLenRestrict($str)));
    return containsHtml($str) ? "" : $strOut;                                   // místo řetězců obsahujících HTML (těla e-mailů apod.) vrátí prázdný řetězec
}
*/
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
function htmlNoAccentsAndTags ($str, $replacedWith = " | ") {                   // odebrání diakritiky a HTML tagů z HTML řetězce (např. pro parsování těl e-mailů do výst. tabzulky 'emails')
    $str = strLenRestrict($str);                                                // omezení délky řetězce   
    $str = str_replace(["\\x0A", "\n", "<br>", "<br />"], $replacedWith, $str ?? ""); // náhrada zalomení řádku oddělovacím řetězcem
    $count = 1;                                                                 // počitadlo provedených náhrad dvou po sobě jdoucích oddělovacích řetězců za jeden
    while ($count > 0) {
        $str = str_replace(str_repeat($replacedWith, 2), $replacedWith, $str ?? "", $count);         
    }                                                                           // odebrání multiplicitních výskytů oddělovacího řetězce, např. " |  |  |  | "  ->  " | "
    $str = strip_tags($str);                                                    // vyřazenmí HTML tagů  
    return removeAccents($str);                                                 // konverze znaků s diakritikou a jiných UTF-8 znaků na znaky bez diakritiky
} 
 */
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function substrInStr ($str, $substr) {                                          // test výskytu podřetězce v řetězci
    return strlen(strstr($str, $substr)) > 0;                                   // vrací true / false
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function mb_ucwords ($str) {                                                    // ucwords pro multibyte kódování
    return mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function convertAddr ($str) {                                                   // nastavení velikosti písmen u adresy (resp. částí dresy)
    global $keywords;
    $addrArrIn  = explode(" ", $str);                                           // vstupní pole slov
    $addrArrOut = [];                                                           // výstupní pole slov
    foreach($addrArrIn as $id => $word) {                                       // iterace slov ve vstupním poli
        switch ($id) {                                                          // $id ... pořadí slova
            case 0:     $addrArrOut[] =  mb_ucwords($word); break;              // u 1. slova jen nastavit velké 1. písmeno a ostatní písmena malá
            default:    $wordLow = mb_strtolower($word, "UTF-8");               // slovo malými písmeny (pro test výskytu slova v poli $keywords aj.)
                        if (in_array($wordLow, $keywords["noConv"])) {
                            $addrArrOut[] = $word;                              // nelze rozhodnout mezi místopis. předložkou a řím. číslem → bez case konverze
                        } elseif (in_array($wordLow, $keywords["addrVal"])) {
                            $addrArrOut[] = $wordLow;                           // místopisné předložky a místopisná označení malými písmeny
                        } elseif (in_array($wordLow, $keywords["romnVal"])) {
                            $addrArrOut[] = strtoupper($word);                  // římská čísla velkými znaky
                        } else {
                            $addrArrOut[] = mb_ucwords($word);                  // 2. a další slovo, pokud není uvedeno v $keywords
                        }
        }
    }
    return implode(" ", $addrArrOut);
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function convertDate ($dateStr) {                                               // konverze data různého (i neznámého) formátu na požadovaný formát
    if (empty($dateStr)) { return ""; }                                         // prázdné datum (např. ve form. polích) vrátit jako prázdný řetězec (jinak ho třída DateTime změní na aktuální datum!)
    if (strlen($dateStr) <= 12) {$dateStr = str_replace(" ","",$dateStr ?? "");}// odebrání mezer u data do délky dd. mm. rrrr (12 znaků)
    $dateStr = preg_replace("/_/", "-", $dateStr ?? "");                        // náhrada případných podtržítek pomlčkami
    try {
        $date = new DateTime($dateStr);                                         // pokus o vytvoření objektu $date jako instance třídy DateTime z $dateStr
    } catch (Exception $e) {                                                    // $dateStr nevyhovuje konstruktoru třídy DateTime ...  
        return $dateStr;                                                        // ... vrátí původní datumový řetězec (nelze převést na požadovaný tvar)
    }                                                                           // $dateStr vyhovuje konstruktoru třídy DateTime ...  
    return $date -> format( (!strpos($dateStr, "/") ? 'Y-m-d' : 'Y-d-m') );     // ... vrátí rrrr-mm-dd (u delimiteru '/' je třeba prohodit m ↔ d)
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function convertMail ($mail) {                                                  // validace e-mailové adresy a převod na malá písmena
    $mail = strtolower($mail);                                                  // převod e-mailové adresy na malá písmena
    $isValid = !(!filter_var($mail, FILTER_VALIDATE_EMAIL)) || empty($mail);    // validace e-mailové adresy (nevyplněná se bere jako validní!)
    return $isValid ? $mail : "(nevalidní e-mail) ".$mail;                      // vrátí buď e-mail (lowercase), nebo e-mail s prefixem "(nevalidní e-mail) "
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function convertPSC ($str) {                                                    // vrátí buď PSČ ve tvaru xxx xx (validní), nebo "nevalidní PSČ ve formuláři"
    $str = str_replace(" ", "", $str ?? "");                                    // odebrání mezer => pracovní tvar validního PSČ je xxxxx
    return (is_numeric($str) && strlen($str) == 5) ? substr($str, 0, 3)." ".substr($str, 3, 2) : "nevalidní PSČ ve formuláři";  // finální tvar PSČ je xxx xx
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function convertFieldValue ($idfield, $val) {                                   // validace + případná normalizace hodnot formulářových polí
    global $fields, $keywords;                                                  // $key = název klíče form. pole; $val = hodnota form. pole určená k validaci
    $titleLow = mb_strtolower($fields[$idfield]["title"], "UTF-8");             // title malými písmeny (jen pro test výskytu klíčových slov v title)                                                                             
    if (in_array($titleLow, $keywords["dateEq"])) {return convertDate($val);}
    if (in_array($titleLow, $keywords["mailEq"])) {return convertMail($val);} 
    foreach ( [ /*"date",*/ "name","addr" /*,"psc"*/] as $valType) {            // "date" se od 03.02.2021 na žádost reportistů nenormalizuje (ošetří si v reportech), někdy jde o pole typu text obsahující např. datum a čas
        foreach ($keywords[$valType] as $substr) {                              // "psc"  se od 13.10.2021 na žádost reportistů nenormalizuje (ošetří si v reportech), normalizace vadí např. u polských PSČ nebo PSČ s pomlčkou
            switch ($valType) {
                //case "date":  if (substrInStr($titleLow, $substr)) {return convertDate($val);}    continue 2;
                case "name":    if (substrInStr($titleLow, $substr)) {return mb_ucwords($val) ;}    continue 2;
                case "addr":    if (substrInStr($titleLow, $substr)) {return convertAddr($val);}    continue 2;
                //case "psc" :  if (substrInStr($titleLow, $substr)) {return convertPSC($val) ;}    continue 2;
            }
        }
    }
    return $val;        // hodnota nepodléhající validaci a korekci (žádná část title form. pole není v $keywords[$valType]
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function convertLogin ($str) {                                                  // je-li loginem (v Daktele users.name) x-kód (platí u Daktely v6), odstraní se počáteční "x"
    return preg_match("/^[xX][0-9]{5}$/", $str) ? preg_replace("/^[xX]/", "", $str ?? "") : $str;
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function boolValsUnify ($val) {         // dvojici booleovských hodnot ("",1) u v6 převede na dvojici hodnot (0,1) používanou u v5 (lze použít u booleovských atributů)
    global $inst;
    switch ($inst["ver"]) {
        case 5: return $val;                    // v5 - hodnoty 0, 1 → propíší se
        case 6: return $val=="1" ? $val : "0";  // v6 - hodnoty "",1 → protože jde o booleovskou proměnnou, nahradí se "" nulami
    }
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function actionCodeToName ($actCode) {  // atribut "action" typu ENUM - převod číselného kódu akce na název akce
    global $campRecordsActions;
    return array_key_exists($actCode, $campRecordsActions) ? $campRecordsActions[$actCode] : $actCode;
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function setFieldsShift () {            // posun v ID formCrmFields oproti ID formFields
    global $formFieldsIdShift, $formCrmFieldsIdShift, $idFormat;
    /* varianta pro striktně numerické číslování instancí (0÷9):
    $formFieldsIdShift    = 0;
    $formCrmFieldsIdShift = pow(10, $idFormat["idTab"] - 1);   // přidá v indexu číslici 1 na první pozici zleva (číslice nejvyššího řádu)
    */
    // varianta pro alfanumerické číslování instancí (0÷9, A, B, ...):
    $formFieldsIdShift    = "0";
    $formCrmFieldsIdShift = "1";
}
// ==============================================================================================================================================================================================
function initGroups () {                // nastavení výchozích hodnot proměnných popisujících skupiny
    global $groups, $idGroup, $tabItems;
    $groups             = [];           // 1D-pole skupin - prvek pole má tvar groupName => idgroup
    $idGroup            = 0;            // umělý inkrementální index pro číslování skupin
    $tabItems["groups"] = 0;            // vynulování počitadla záznamů v tabulce 'groups'
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function initDbGroups () {              // nastavení výchozích hodnot proměnných popisujících skupiny
    global $dbGroups, $idDbGroup, $tabItems, $emptyToNA, $fakeId;
    $dbGroups = $emptyToNA?[$fakeId]:[];// 1D-pole skupin databází - prvek pole má tvar dbGroupName => iddatabasegroup
    $idDbGroup            = 0;          // umělý inkrementální index pro číslování skupin databází
    $tabItems["dbGroups"] = 0;          // vynulování počitadla záznamů v tabulce 'groups'
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function initStatuses () {              // nastavení výchozích hodnot proměnných popisujících stavy
    global $statuses, $idStatus, $idstatusFormated, $tabItems;
    $statuses = [];                     /* 3D-pole stavů - prvek pole má tvar  <statusId> => ["title" => <hodnota>, "statusIdOrig" => [pole hodnot]],
                                           kde statusId a title jsou unikátní, statusId jsou neformátované indexy (bez prefixu instance, který v commonStatus
                                           režimu nemá význam, a bez formátování na počet číslic požadovaný ve výstupních tabulkách)
                                           a v poli statusIdOrig jsou originální (prefixované) ID stejnojmenných stavů z různých instancí  */
    $idStatus             = 0;          // umělý inkrementální index pro číslování stavů (1, 2, ...)
    $tabItems["statuses"] = 0;          // vynulování počitadla záznamů v tabulce 'statuses'
    unset($idstatusFormated);           // formátovaný umělý index stavu ($idStatus doplněný na počet číslic požadovaný ve výstupních tabulkách)
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function initFields () {                // nastavení výchozích hodnot proměnných popisujících formulářová pole
    global $fields;
    $fields = [];                       // 2D-pole formulářových polí - prvek pole má tvar <idfield> => ["name" => <hodnota>, "title" => <hodnota>]  
}                                       // pole je vytvářeno postupně zvlášť pro každou instanci
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function iterStatuses ($val, $valType = "statusIdOrig") {               // prohledání 3D-pole stavů $statuses
    global $statuses, $emptyToNA, $fakeId;                              // $val = hledaná hodnota;  $valType = "title" / "statusIdOrig"
    if ($valType=="statusIdOrig" && $emptyToNA && $val==$fakeId) {
        return $fakeId;                                                 // původně prázdná hodnota FK je nahrazena hodnotou $fakeId (typicky "n/a")
    }
    foreach ($statuses as $statId => $statRow) {
        switch ($valType) {
            case "title":           // $statRow[$valType] je string
                                    if ($statRow[$valType] == $val) {   // zadaná hodnota v poli $statuses nalezena
                                        return $statId;                 // ... → vrátí id (umělé) položky pole $statuses, v níž se hodnota nachází
                                    }
                                    break;
            case "statusIdOrig":    // $statRow[$valType] je 1D-pole
                                    foreach ($statRow[$valType] as $statVal) {
                                        if ($statVal == $val) {         // zadaná hodnota v poli $statuses nalezena
                                            return $statId;             // ... → vrátí id (umělé) položky pole $statuses, v níž se hodnota nachází
                                        }
                                    }
        }        
    }
    return false;                   // zadaná hodnota v poli $statuses nenalezena
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function dateRngCheck ($val) {                                          // $val je datumočas ze zkoumaného záznamu
    global $incrementalOn, $processedDates;
    $rowDate   = substr($val, 0, 10);                                   // datum ze zkoumaného záznamu
    return ($incrementalOn && ($rowDate < $processedDates["start"] || $rowDate > $processedDates["end"])) ? false : true;
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function emptyToNA ($id) {          // prázdné hodnoty nahradí hodnotou $fakeId - kvůli GoodData, aby zde byla nabídka $fakeTitle [volitelné]
    global $emptyToNA, $fakeId;
    return ($emptyToNA && empty($id)) ? $fakeId : $id;
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function checkIdLengthOverflow ($val) {     // kontrola, zda došlo (true) nebo nedošlo (false) k přetečení délky ID určené proměnnou $idFormat["idTab"] ...
    global $idFormat, $tab;                 // ... nebo umělým ID (groups, statuses, fieldValues)
        if ($val >= pow(10, $idFormat["idTab"])) {
            logInfo("PŘETEČENÍ DÉLKY INDEXŮ ZÁZNAMŮ V TABULCE ".$tab);          // volitelný diagnostický výstup do logu                
            $idFormat["idTab"]++;
            $digitsNo = $idFormat["instId"] + $idFormat["idTab"];
            logInfo("DÉLKA INDEXŮ NAVÝŠENA NA ".$digitsNo." ČÍSLIC");
            return true;                    // došlo k přetečení → je třeba začít plnit OUT tabulky znovu, s delšími ID
        }
    return false;                           // nedošlo k přetečení (OK)
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function getJsonItem ($str, $key) {         // získání konkrétního prvku z JSON řetězce
    $decod = json_decode($str, true, JSON_UNESCAPED_UNICODE);                   // dekódovaný řetězec (je-li $str JSON, je $decod ARRAY)
    if (is_null ($decod)) {return [];}                                          // neobsahuje-li JSON žádné položky <> NULL, vrátí prázdné pole
    if (is_array($decod)) {
        if (array_key_exists($key, $decod)) {
            return $decod[$key];                                                // $decod[$key] je STRING, INT, BOOL nebo ARRAY (dle struktury JSONu), nejčastěji ARRAY
        }
    } else return $decod;                                                       // pokud by $decod nebylo ARRAY, ale jednoduchá hodnota (STRING/INT/BOOL), vrátí tuto hodnotu
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function jsonParseActivit ($formArr, $parentKey = '') {  // formArr je vícerozměrné asociativní pole (dimenze pole není předem přesně známá) -> nutno rekurzivně prohledat (nebo ignorovat vnořená pole)
    global $jsonFieldsOuts, $jsonFieldsOutsEmpty, $tab, $out_actItems, $actItems, $idactivity, $idFormat, $instId, $jsonParse;
                                                                            // $tab = "activities", $jsonFieldsOuts[$tab] = "actItemVals", $jsonFieldsOutsEmpty[$tab] = "actItemValsEmpty"
    global ${"out_".$jsonFieldsOuts[$tab]};                                 // název out-only tabulky pro zápis hodnot JSON polí
    $eefi = array_key_exists($tab, $jsonFieldsOutsEmpty);                   // eefi = exportEmptyFieldsIdent ... příznak zda se má exportovat seznam prázdných hodnot do speciální výstupní tabulky    
    if ($eefi) {
        global ${"out_".$jsonFieldsOutsEmpty[$tab]};                        // název out-only tabulky pro zápis seznamu prázdných hodnot JSON polí
    }
    
    foreach ($formArr as $key => $val) {                                    // $val je buď string, nebo vnořené ?D-pole (parsování vnořených atributů je volitelné)
        $keyChained = blanc($parentKey) ? $key : $parentKey.".".$key;       // názvy klíčů vnořených polí jsou oddělovány tečkou (používá se u vnořených atributů)                                                    
        // $val je neprázdné pole
        if (is_array($val)) {                                               // $val je pole → moho existovat vnořené atributy
            if (!blanc($val) && $jsonParse["activities.item_parseNestedAttrs"]) {   // parsování vnořených ?D-polí je zapnuto →  
                jsonParseActivit($val, $keyChained);                                // → prohledává se dále rekurzivním voláním fce                
            }
            continue;
        }
        // ----------------------------------------------------------------------------------------------------------------------------------
        // $val není pole, ale jednoduchá hodnota
        // hledání ID parametru z activities.item ($idactitem) v poli $actItems; při nenalezení je parametr přidán do pole $actItem a out-only tabulky "actItems"
        if (array_key_exists($keyChained, $actItems)) {
            $idactitem = $actItems[$keyChained];                            // název parametru z activities.item byl nalezen v poli $actItems     
        } else {                                                            // název parametru z activities.item nebyl nalezen v poli $actItems
            $idactitem = setIdLength(0, count($actItems) + 1, false);       // parametr z activities.item přidávaný na seznam bude mít ID o 1 vyšší než je stávající počet prvků $actItems
            $actItems[$keyChained] = $idactitem;                            // přidání definice parametru z activities.item do pole $actItems
            $out_actItems -> writeRow([$idactitem, $keyChained]);           // přidání definice parametru z activities.item do out-only tabulky "actItems"
        } // --------------------------------------------------------------------------------------------------------------------------------         
        if (is_string($val)) {
            $val = strLenRestrict_htmlThrow_remStrMultipl_trimAll($val);    // normalizovaná hodnota - vyřazeny texty obsahující HTML, bez multiplicitního výskytu podřetězců, přebytečných mezer, ořezaná
        }
        
        if (!blanc($val)) {                                                 // k danému klíči existuje neprázdná hodnota
            $actItemVals = [                                                // záznam do out-only tabulky hodnot z activities.item ("actItemVals")
                $idactivity . $idactitem,                                   // ID cílového záznamu do out-only tabulky hodnot z activities.item ("actItemVals")
                $idactivity,                                                // ID zdrojové aktivity obsahující parsovaný JSON
                $idactitem,                                                 // idactitem
                $val                                                        // korigovaná hodnota parametru z activities.item
            ];                                                                                                                                                                     
            ${"out_".$jsonFieldsOuts[$tab]} -> writeRow($actItemVals);      // zápis řádku do out-only tabulky hodnot formulářových polí 
            return;
        }
        
        if (blanc($val) && $eefi) {                                         // prázdná hodnota && seznam prázdných hodnot se má exportovat
            $emptyActItemIdent = [                                          // údaje o záznamu do out-only tabulky hodnot z activities.item ("actItemVals"), který má prázdnou hodnotu
                $idactivity . $idactitem,                                   // ID cílového záznamu do out-only tabulky hodnot z activities.item ("actItemVals")
                $idactivity,                                                // ID zdrojové aktivity obsahující parsovaný JSON
                $idactitem,                                                 // idactitem
                $key                                                        // systémové označení JSON atributu - název klíče (pro lepší orientaci při ručním porovnávání s JSONy v IN tabulkách
            ];                                                                                                                                                                     
            ${"out_".$jsonFieldsOutsEmpty[$tab]} -> writeRow(emptyActItemIdent);    // zápis řádku do out-only tabulky představující seznam prázdných hodnot formulářových polí             
        }
    }
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function jsonItemOneValueProcess ($key, $val, $idVal, $lastChild, $eefi) {
    global $jsonFieldsOuts, $jsonFieldsOutsEmpty, $tab, $colName, $idFieldSrcRec, $idFormat, $fields, $instId, $jsonParse, $adhocDump, $dumpColList;
    global ${"out_".$jsonFieldsOuts[$tab]};                                 // název out-only tabulky pro zápis hodnot formulářových polí    
    if ($eefi) { global ${"out_".$jsonFieldsOutsEmpty[$tab]}; }             // eefi = exportEmptyFieldsIdent;  název out-only tabulky pro zápis seznamu prázdných hodnot formulářových polí

    
    if (!blanc($val)) {
        // optimalizace hodnot formulářových polí
        if (is_string($val)) {
            $val = strLenRestrict_htmlThrow_remStrMultipl_trimAll($val);    // normalizovaná hodnota - vyřazeny texty obsahující HTML, bez multiplicitního výskytu podřetězců, přebytečných mezer, ořezaná
        }
        // ----------------------------------------------------------------------------------------------------------------------------------
        // validace a korekce hodnoty formulářového pole + konstrukce řádku out-only tabulky hodnot formulářových polí    
        if ($idVal == pow(10, $idFormat["idField"])) {                      // došlo k přetečení délky indexů hodnot form. polí
        logInfo("PŘETEČENÍ DÉLKY INDEXU HODNOT FORM. POLÍ V TABULCE ".$tab);// volitelný diagnostický výstup do logu
        $idFormat["idField"]++;            
        }   // výstupy se nezačínají plnit znovu od začátku, jen se navýší počet číslic ID hodnot form. polí od dotčeného místa dále
    }
    // --------------------------------------------------------------------------------------------------------------------------------------
    
    $idfield = "";
    foreach ($fields as $idfi => $field) {                                  // v poli $fields dohledám 'idfield' ke známému 'name'
        //$instDig       = floor($idfi/pow(10, $idFormat["idTab"]));        // číslice vyjadřující ID aktuálně zpracovávané instance
        //$fieldShiftDig = floor($idfi/pow(10, $idFormat["idTab"]-1)) - 10* $instId;  // číslice vyjadřující posun indexace crmFields vůči fields (0/1) - funguje jen u inst. 1÷9 (integer)
        $fieldShiftDig = substr($idfi, $idFormat["instId"], 1);             // číslice vyjadřující posun indexace crmFields vůči fields (0/1) - 2. číslice v $idfi zleva
        //if ($instDig != $instId) {continue;}                              // nejedná se o formulářové pole z aktuálně zpracovávané instance
        if (($tab == "crmRecords" && $fieldShiftDig == 0) ||
            ($tab != "crmRecords" && $fieldShiftDig == 1) ) {continue;}     // výběr form. polí odpovídajícího původu (crmFields/fields) pro daný typ tabulky
        if ($field["name"] == $key) {
            logInfo($tab." - NALEZENO PREFEROVANÉ FORM. POLE [".$idfi.", ".$field['name'].", ".$field['title']."]", "jsonParseInfo");
            $idfield = $idfi; break;
        }
    }
    if ($idfield == "") {   // nebylo-li nalezeno form. pole odpovídajícího name, pokračuje hledání v druhém z typů form. polí (fields/crmFields)
        logInfo($tab." - NENALEZENO PREFEROVANÉ FORM. POLE -> ", "jsonParseInfo");  // diag. výstup do logu
        foreach ($fields as $idfi => $field) {
            //$instDig       = floor($idfi/pow(10, $idFormat["idTab"]));    // číslice vyjadřující ID aktuálně zpracovávané instance
            //$fieldShiftDig = floor($idfi/pow(10, $idFormat["idTab"]-1)) - 10* $instId; // číslice vyjadřující posun indexace crmFields vůči fields (0/1) - funguje jen u inst. 1÷9 (integer)
            $fieldShiftDig = substr($idfi, $idFormat["instId"], 1);         // číslice vyjadřující posun indexace crmFields vůči fields (0/1) - 2. číslice v $idfi zleva            
            //if ($instDig != $instId) {continue;}                          // nejedná se o formulářové pole z aktuálně zpracovávané instance
            if (($tab == "crmRecords" && $fieldShiftDig == 1) ||
                ($tab != "crmRecords" && $fieldShiftDig == 0) ) {continue;} // výběr form. polí odpovídajícího původu
            if ($field["name"] == $key) {
                logInfo("  ALTERNATIVNÍ POLE JE [".$idfi.", ".$field['name'].", ".$field['title']."]", "jsonParseInfo");
                $idfield = $idfi; break;
            }
        }
    } // --------------------------------------------------------------------------------------------------------------------------------
    // zde už je známa hodnota $idfield pro aktuálně iterované formulářové pole    
    if (is_string($val)) { $val = convertFieldValue($idfield, $val); }      // je-li část názvu klíče $key v klíč. slovech $keywords, vrátí validovanou/konvertovanou hodnotu, jinak nezměněnou $val  
    
    if (!blanc($val)) {                                                     // k danému klíči existuje neprázdná hodnota        
        $fieldVals = [                                                      // záznam do out-only tabulky hodnot formulářových polí
            $idFieldSrcRec . $idfield . setIdLength(0,$idVal,false,"field"),// ID cílového záznamu do out-only tabulky hodnot formulářových polí
            $idFieldSrcRec,                                                 // ID zdrojového záznamu z tabulky obsahující parsovaný JSON
            $idfield,                                                       // idfield
            $val                                                            // korigovaná hodnota formulářového pole
        ];                                                                                                                                                                     
        ${"out_".$jsonFieldsOuts[$tab]} -> writeRow($fieldVals);            // zápis řádku do out-only tabulky hodnot formulářových polí
        if ($adhocDump["active"]) {
            if ($colName == $dumpColList[$instId][$tab]["dumpColName"]) {   // pravá strana je pole → nelze ho generovat fcí arrVal (ta to neumí)
                if (in_array($idFieldSrcRec, $dumpColList[$instId][$tab]["dumpColVals"])) {     // 2. argument je pole → nelze ho generovat fcí arrVal (ta to neumí)
                    echo $instId."_".$tab." - ADHOC DUMP (\$key = ".$key."): [idVal ".$fieldVals[0].", idSrcRec ".$fieldVals[1].", idfield ".$fieldVals[2].", val ".$fieldVals[3]."]\n";
                }
            }
        }    
    }
    
    if (blanc($val) && $lastChild && $eefi) {                               // prázdná hodnota && jde o poslední hodnotu k danému klíči (z obecně více možných hodnot) && seznam prázdných hodnot se má exportovat        
        $emptyFieldIdent = [                                                // záznam do out-only tabulky představující seznam formulářových polí s prázdnou hodnotou
            $idFieldSrcRec . $idfield,                                      // ID cílového záznamu do out-only tabulky hodnot formulářových polí
            $idFieldSrcRec,                                                 // ID zdrojového záznamu z tabulky obsahující parsovaný JSON
            $idfield,                                                       // idfield
            $key                                                            // systémové označení JSON atributu - název klíče (pro lepší orientaci při ručním porovnávání s JSONy v IN tabulkách
        ];                                                                                                                                                                     
        ${"out_".$jsonFieldsOutsEmpty[$tab]} -> writeRow($emptyFieldIdent); // zápis řádku do out-only tabulky představující seznam prázdných hodnot formulářových polí
        if ($adhocDump["active"]) {
            if ($colName == $dumpColList[$instId][$tab]["dumpColName"]) {   // pravá strana je pole → nelze ho generovat fcí arrVal (ta to neumí)
                if (in_array($idFieldSrcRec, $dumpColList[$instId][$tab]["dumpColVals"])) {     // 2. argument je pole → nelze ho generovat fcí arrVal (ta to neumí)
                    echo $tab." - ADHOC DUMP (\$key = ".$key."): [idVal ".$emptyFieldIdent[0].", idSrcRec ".$emptyFieldIdent[1].", idfield ".$emptyFieldIdent[2]."]\n";
                                                                            // k danému klíči jsou všechny hodnoty prázdné → byl o tom učiněn záznam do výstupní tabulky
                }
            }
        }
    }
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function jsonParse ($formArr) {             // formArr je 2D-pole    
    global $jsonFieldsOutsEmpty, $tab, $jsonParse;
    
    $eefi = array_key_exists($tab, $jsonFieldsOutsEmpty);                       // exportEmptyFieldsIdent - příznak zda se má exportovat seznam prázdných hodnot do speciální výstupní tabulky
    
    foreach ($formArr as $key => $valArr) {                                     // $valArr je 1D-pole, obvykle má jen klíč 0 (nebo žádný)  
        
        // 1D-pole $valArr neobsahuje žádnou hodnotu (může jít o dříve existující smazanou hodnotu)
        if (blanc($valArr)) {                                                   // nevyplněné formulářové pole - neobsahuje žádný prvek                                                         
            if ($eefi) { jsonItemOneValueProcess ($key, "", 1, true, $eefi); }  // 1 ... prázdná hodnota je jediná;  true ... zpracovávat jako "last child";  vložení jedné hodnoty "" (prázdný řetězec)
            continue;
        }
        
        // 1D-pole $valArr obsahuje alespoň jednu hodnotu (nejčastěji je u neprázdných polí PRÁVĚ jedna)
        $idVal = 0;                                                             // ID hodnoty konkrétního form. pole  
        foreach ($valArr as $val) {                                             // klíč = 0,1,... (nezajímavé); $val jsou hodnoty form. polí
            if (is_array($val)) {continue;}                                     // ošetření případu, že by jednotlivou hodnotou bylo pole → v takovém případě hodnotu ignorovat
            $idVal++;            
            $lastChild = ($idVal == count($valArr));            
            if (!blanc($val) || ($lastChild && $eefi)) {                        // prázdné hodnoty dále zpracováváme jen jde-li o poslední hodnotu k danému klíči a má-li se seznam prázdných hodnot exportovat
                jsonItemOneValueProcess ($key, $val, $idVal, $lastChild, $eefi);
            }
        }
    }
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function jsonProcessing ($instId, $tab, $colName, $hodnota) {                   // test, zda je ve sloupci JSON; když ano, rozparsuje se
    global $jsonList;
    if (!is_null(arrVal([$instId, $tab, $colName], $jsonList))) {               // sloupec obsahuje JSON
        $formArr = json_decode($hodnota, true, JSON_UNESCAPED_UNICODE);
        if (is_array($formArr)) {                                               // hodnota dekódovaného JSONu je pole → lze ji prohledávat jako pole (pův. podmínka jen !is_null)
            switch ($tab) {
              case "activities":  jsonParseActivit($formArr); break;            // uplatní se u tabulky "activities"
              default:            jsonParse($formArr);                          // uplatní se u tabulek "records", "crmRecords", "contact" a "tickets"
            }
        }                                
        return $jsonList[$instId][$tab][$colName] ? true : false;               // buňka obsahovala JSON; po návratu z fce pokračovat/nepokračovat ve zpracování hodnoty
    }
    return true;                                                                // buňka neobsahovala JSON, po návratu z fce pokračovat ve zpracování hodnoty
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function dateRestrictColId ($instId, $tab) {                                    // nalezení ID sloupce pro datumovou restrikci záznamů (pokud takový sloupec v tabulce existuje)
    global $tiList;
    return arrVal([$instId, $tab], $tiList);                                    // při nenalezení ID sloupce pro datumovou restrikci (např. u statických tabulek) vrátí NULL
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function colParentTab ($instId, $tab, $colName) {                               // nalezení názvu nadřazené tabulky pro daný sloupec (je-li sloupec FK)
    global $fkList;
    //echo " | count(\$fkList) = ".count($fkList);                              // diagnostika
    return arrVal([$instId, $tab, $colName], $fkList);                          // není-li daný sloupec FK, vrátí NULL
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function channelsApiGet ($idcall) {                                             // k danému "id_call" z activities.item stáhne přes API pole "channels"
    global $instances, $instId;
    $ch = curl_init();                                                          // API dotaz - začátek
    $urlParams = ["accessToken" => $instances[$instId]["accessToken"] ];        // parametry URL adresy
    $url  = $instances[$instId]["url"];                                         // konstrukce URL adresy
    $url .= "/api/v6/activitiesCall/";
    $url .= $idcall;
    $url .= "/channels.json?" . http_build_query($urlParams);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
    $response = curl_exec($ch);                                                 // volání API dotazu → vrátí JSON
    $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);                                                            // API dozaz - konec
    return $httpStatus == 200 ? arrVal(["result", "data"], json_decode($response, true, JSON_UNESCAPED_UNICODE), true) : null;
}   // fce vrací pole hovorových kanálů s klíči 0 (celá aktivita), 1 (konverzace s agentem), popř. 2 (přepojená část hovoru, existuje-li)
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function channelsParse ($item = []) {                                           // parsování sub-JSONu activities.item.channels
    global $instances, $instId, $fakeId;

    if (!empty(arrVal("channels", $item, true))) {                              // je-li v activities.item vyplněn prvek "channels" (ver. <= 6.17) ...
        $channelsArr = arrVal("channels", $item, true);                         // ... použij jako channelsData tento prvek [true ... návratová hodnota je typu array], ...
    } else if ($instances[$instId]["channelsApiGet"]) {                         // ... jinak, pokud je to pro danou instanci zapnuto v konfiguračním JSONu, ...
        $channelsArr = channelsApiGet(arrVal("id_call", $item));                // ... stáhni channelsData API dotazem z endpointu "/api/v6/activitiesCall/[id_call]/channels.json", ...
    } else {                                                                    // ... kde [id_call] = $item["id_call"]  (ver. >= 6.18), ...
        $channelsArr = [];                                                      // ... jinak nech pole kanálů prázdné
    }
    if (empty($channelsArr)) {                                                  // v poli $item ani na endpointu "/channels.json" klíč "channels" buď neexistuje, nebo je na obou místech "channels" =  NULL
        return  [   "idrecord"          => $fakeId,
                    "idstatus"          => $fakeId,
                    "duration_channels" => 0,
                    "transfered"        => false,
                    "transfer_number"   => ""
                ];        
    }
    
    $channels = [];                                                             // výběr podstatných parametrů z parsovaného sub-JSONu activities.item.channels (indexy pole jsou 0, 1, u přepojených hovorů i 2)
    foreach ($channelsArr as $channelData) {    // $channels[0] = celkové parametry relace, $channels[1] = parametry hovoru operátora, $channels[2] = parametry přepojeného hovoru (existuje-li); [true ... návratová hodnota je typu array]
        if (is_array($channelData) && !empty($channelData)) {
            $channelId    = arrVal(["_sys", "id"]                           , $channelData);
            $idRec        = arrVal(["activity", "record", "_sys", "id"]     , $channelData);
            $idStat       = arrVal(["activity", "statuses", 0, "_sys", "id"], $channelData);
            $timeSpeaking = arrVal("time_speaking"                          , $channelData);
            $timeClosed   = arrVal("time_closed"                            , $channelData);
            $clid         = arrVal("clid"                                   , $channelData);
            
            if(is_null($timeSpeaking)) { continue; }                            // kanály s nevyplněným time_speaking vynechat (mohou se vyskytovat u některých aktivit v Daktele ver. >= 6.17)
            
            $channels[] = [ "id"                => $channelId,                                                                                                      // 'strtotime' vrátí Unix timestamp nebo false; ...
                            "idrec"             => emptyToNA($idRec),
                            "idstat"            => emptyToNA($idStat),
                            "duration_channels" => !strtotime($timeClosed) || !strtotime($timeSpeaking) ? 0 : strtotime($timeClosed) - strtotime($timeSpeaking),    // ... je-li jedno z čísel nedef., duration je 0 (aby nevznikaly velké hodnoty jako 0-UnixTimestamp)
                            "clid"              => phoneNumberCanonic($clid)
                          ];
        }    
    }
    array_multisort(array_column($channels, "id"), SORT_ASC, SORT_NUMERIC, $channels);  // seřazení prvků pole $channels podle "_sys"."id" channelu (jedná se o autoinkrementální index čísla kanálu v Daktele)
    
    // návratové pole
    return  [ "idrecord"          => arrVal([1, "idrec"],               $channels),     // idrecord braný z item.channels
              "idstatus"          => arrVal([1, "idstat"],              $channels),     // idstatus braný z item.channels
              "duration_channels" => arrVal([1, "duration_channels"],   $channels, 0 ), // modifikovaný čas tvrvání hovoru vypočtený z channelu 1 (agent) [s]
              "transfered"        => array_key_exists(2,                $channels),     // příznak zda šlo o přepojený hovor (true/false)
              "transfer_number"   => arrVal([2, "clid"],                $channels, ""), // cílové tel. číslo v kanonickém tvaru (u přepojených hovorů)
            ];
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function getACW ($timeOpen, $timeClose, $duration) {                    // výpočet doby ACW (After Call Work) u aktivit typu CALL
    // je-li $timeOpen nebo $timeClose nedef., vrátí ACW = 0 (aby nevznikaly velké hodnoty jako 0-UnixTimestamp)
    $acw = !strtotime($timeOpen) || !strtotime($timeClose) ? 0 : strtotime($timeClose) - strtotime($timeOpen) - (int)$duration;
    return max($acw, 0);        // záporné hodnoty nahradit nulou (dělá to ve statistikách i Daktela) 
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function getACWout ($timeClose, $callTime, $ringingTime, $duration) {   // výpočet doby ACW (After Call Work) u aktivit typu CALL pro odchozí hovory
    // je-li $timeClose nebo $callTime nedef., vrátí ACWout = 0 (aby nevznikaly velké hodnoty jako 0-UnixTimestamp)
    $acwOut = !strtotime($timeClose) || !strtotime($callTime) ? 0 : strtotime($timeClose) - (strtotime($callTime) + (int)$ringingTime + (int)$duration);
    return max($acwOut, 0);     // záporné hodnoty nahradit nulou (dělá to ve statistikách i Daktela) 
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function getBCW ($callTime, $timeOpen) {                                // výpočet doby BCW (Before Call Work) u aktivit typu CALL
    // je-li $callTime nebo $timeOpen nedef., vrátí BCW = 0 (aby nevznikaly velké hodnoty jako 0-UnixTimestamp)
    $bcw = !strtotime($callTime) || !strtotime($timeOpen) ? 0 : strtotime($callTime) - strtotime($timeOpen);
    return max($bcw, 0);        // záporné hodnoty nahradit nulou (dělá to ve statistikách i Daktela) 
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function adhocDump ($instId, $tab, $colVals, $derivedVals = []) {
    // $derivedVals = [] (u neodvozených tabulek) / $callsVals, $emailsVals, $smsVals, $chatsVals (u odvozených tabulek)
    global $adhocDump, $dumpColList;   
    if (!$adhocDump["active"]) { return; }                              // dumpování není zapnuto v konfiguračním JSONu v KBC
    if (!array_key_exists($instId, $dumpColList)) { return; }           // pro aktuální instanci není požadován žádný dump
    if (!array_key_exists($tab, $dumpColList[$instId])) { return; }     // pro aktuální tabulku v dané instanci není požadován žádný dump
    if (!array_key_exists("dumpColVals", $dumpColList[$instId][$tab])) { return; }
    if (!array_key_exists("dumpColId"  , $dumpColList[$instId][$tab])) { return; }    
    $dumpColVals = $dumpColList[$instId][$tab]["dumpColVals"];          // 1D-pole hodnot, které mají být dumpovány; pravá strana je pole → nelze ho generovat fcí arrVal (ta to neumí)
    if (!is_array($dumpColVals) || empty($dumpColVals)) { return; }
    $dumpColId = $dumpColList[$instId][$tab]["dumpColId"];          // zde nejde použít fci arrVal (hodnota nepředstavuje poslední úroveň pole $dumpColList)
    $dumpColVal = $colVals[$dumpColId];                             // hodnota dumpovacího sloupce v daném záznamu (řádku)
    if (in_array($dumpColVal, $dumpColVals)) {                      // tento záznam má být dumpován
        //logInfo($dumpColVal); print_r($dumpColVals);
        switch(empty($derivedVals)) {
            // výpis řádku in/out tabulky (neodvozené tabulky)
            case true:  echo "ADHOC DUMP (".$instId."_".$tab." / ".$dumpColVal."): ["; 
                        foreach ($colVals as $key => $val) {        // $key = 0, 1, ... , count($colVals)-1
                            echo $val;
                            echo $key < count($colVals)-1 ? " | " : "]\n\n";
                        }
                        break;
            // výpis řádku odvozené tabulky (call, email, sms, chat)
            case false: echo "ADHOC DUMP (".$instId."_".$tab." / ".$dumpColVal.") -> DERIVED TABLE VALUES: ["; 
                        foreach ($derivedVals as $key => $derivedVal) { // $key = 0, 1, ... , count($derivedVals)-1
                            echo $derivedVal;
                            echo $key < count($derivedVals)-1 ? " | " : "]\n\n";
                        } 
                        break;                    
        }
    }
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------