<?php
// proměnné a konstanty

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// datumový rozsah zpracování
$processedDates     =   [   "start" =>  date("Y-m-d", strtotime(-$histDays['start']." days +2 hours")),     // počáteční datum zpracováváného rozsahu
                            "end"   =>  date("Y-m-d", strtotime(-$histDays['end']  ." days +2 hours"))      // koncové datum zpracovávaného rozsahu
                        ];              // +2 hours ... korekce UTC → UTC+1(2) [jinak je při běhu transformace krátce po půlnoci datum o den nižší]
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// seznam instancí Daktela
$instances = [  "1" =>  ["url" => "https://ilinky.daktela.com",             "ver" => 5, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "2" =>  ["url" => "https://dircom.daktela.com",             "ver" => 5, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "3" =>  ["url" => "https://conectart.daktela.com",          "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "4" =>  ["url" => "https://conectart-in.daktela.com",       "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "5" =>  ["url" => "https://conectart-offsite.daktela.com",  "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "6" =>  ["url" => "https://samsung.daktela.com",            "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "7" =>  ["url" => "https://teleporadce.daktela.com",        "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "8" =>  ["url" => "https://conectart-morava.daktela.com",   "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "9" =>  ["url" => "https://csu.daktela.com",                "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "A" =>  ["url" => "https://driverama.daktela.com",          "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "B" =>  ["url" => "https://ceskaposta.daktela.com",         "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "C" =>  ["url" => "https://mnd.daktela.com",                "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "D" =>  ["url" => "https://wienerberger.daktela.com",       "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL],
                "E" =>  ["url" => "https://unicredit.daktela.com",          "ver" => 6, "accessToken" => NULL, "channelsApiGet" => NULL, "instOn" => NULL]
];
//  poli $instances se nastaví hodnoty klíčů "accessToken" (API token), "instOn" (příznak zapnutého/vypnutého zpracování instance) a "channelsApiGet" dle konfiguračního JSONu:
foreach ($instances as $instId => $instAttrs) {
    $instances[$instId]["accessToken"]    = $accessTokens[$instId];
    $instances[$instId]["channelsApiGet"] = empty($channelsApiGet[$instId])     ? false : true; // příznak, zda se na instanci mají stahovat hovorové kanály (channels) z API endpointu "channels.json"
    $instances[$instId]["instOn"]         = empty($processedInstances[$instId]) ? false : true; // "vypnutí" zpracovávání dané instance znamená, že se zpracovávájí jen statické tabulky
}                                                                                               // [vstupní hodnoty false se vyhodnotí jako empty :) ]
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// struktura tabulek

/* základní požadavky nutné u pořadí tabulek:
        - 'records' a 'recordSnapshots' se odkazují na 'statuses'.'idstatus' → musí být uvedeny až za 'statuses' (pro případ použití commonStatuses)
        - 'records' a 'fieldValues' se tvoří pomocí pole $fields vzniklého z tabulky 'fields' → musí být uvedeny až za 'fields' (kvůli foreach)
   detailní požadavky pořadí tabulek (respektující integritní vazby mezi tabulkami pro správnou funkci integritní validace - stejné jako u writeru):
        skupina 1  -  (groups)*, (databaseGroups)*, (instances)*, statuses                      *  - out-only tabulky, vznikají v transformaci
        skupina 2  -  queues, fields, users, pauses, ivrs**, ticketSla**, crmRecordTypes**      ** - jen u v6  /  *** - jen u v5
        skupina 3  -  accounts**, databases**, ticketCategories**, readySessions**, loginSessions, pauseSessions, queueSessions, idleSessions
        skupina 4  -  contacts**, records, calls***
        skupina 5  -  recordSnapshots, (fieldValues)*, (contFieldVals)*, tickets**
        skupina 6  -  crmRecords**, activities**, (tickFieldVals)*                                     
        skupina 7  -  crmRecordSnapshots**, (crmFieldVals)*, activitiesAttempt**, qaReviews**
*/

// vstupně-výstupní tabulky (načtou se jako vstupy, transformují se a výsledek je zapsán jako výstup)

/* "tab" => [   "instPrf" - prefixovat hodnoty ve sloupci identifikátorem instance (0/1),
                "pk"   (nepovinné) - primární klíč (1),
                "fk"   (nepovinné) - cizí klíč (tabName),
                "tt"   (nepovinné) - sloupec obsahující title (1),
                "json" (nepovinné) - přítomnost klíče indikuje, že jde o JSON; 0/1 = jen rozparsovat / rozparsovat a pokračovat ve zpracování hodnoty (0/1),
                "ti"   (nepovinné) - parametr pro časovou restrikci záznamů (1) - jen u tabulek obsahujících dynamické údaje
                                     [statické se nesmějí datumově restringovat (např. "crmRecordTypes" datem vytvoření), musejí být k dispozici] 
            ]
*/
$tabsInOutV56_part1 = [
    // skupina 1 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "statuses"          =>  [   "idstatus"              =>  ["instPrf" => 1, "pk" => 1],
                                "title"                 =>  ["instPrf" => 0, "tt" => 1]
                            ],
    // skupina 2 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "queues"            =>  [   "idqueue"               =>  ["instPrf" => 1, "pk" => 1],
                                "title"                 =>  ["instPrf" => 0, "tt" => 1],
                                "idinstance"            =>  ["instPrf" => 0, "fk" => "instances"],
                                "idgroup"               =>  ["instPrf" => 0, "fk" => NULL],     // "fk" => "groups" neuvedeno ("groups" je out-only tabulka vytvářená v transformaci z "queues")
                                "outboundcid"           =>  ["instPrf" => 0]
                            ],                                                                  // "idgroup" je v IN tabulce NÁZEV → neprefixovat
    "fields"            =>  [   "idfield"               =>  ["instPrf" => 1, "pk" => 1],
                                "title"                 =>  ["instPrf" => 0, "tt" => 1],
                                "idinstance"            =>  ["instPrf" => 0, "fk" => "instances"],
                                "name"                  =>  ["instPrf" => 0]
                            ],
    "users"             =>  [   "iduser"                =>  ["instPrf" => 1, "pk" => 1],
                                "title"                 =>  ["instPrf" => 0, "tt" => 1],
                                "idinstance"            =>  ["instPrf" => 0, "fk" => "instances"],
                                "email"                 =>  ["instPrf" => 0],
                                "login"                 =>  ["instPrf" => 0],
                                "profiletitle"          =>  ["instPrf" => 0],
                                "roletitle"             =>  ["instPrf" => 0],
                                "location"              =>  ["instPrf" => 0]
                            ],
    "pauses"            =>  [   "idpause"               =>  ["instPrf" => 1, "pk" => 1],
                                "title"                 =>  ["instPrf" => 0],
                                "idinstance"            =>  ["instPrf" => 0, "fk" => "instances"],
                                "type"                  =>  ["instPrf" => 0],
                                "paid"                  =>  ["instPrf" => 0]
                            ]
];
$tabsInOutV6_part1  = [
    // skupina 2 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "ivrs"              =>  [   "idivr"                 =>  ["instPrf" => 1, "pk" => 1],
                                "idivrtype"             =>  ["instPrf" => 0, "pk" => 1],
                                "title"                 =>  ["instPrf" => 0, "tt" => 1],
                                "idinstance"            =>  ["instPrf" => 0, "fk" => "instances"],
                                "timeout"               =>  ["instPrf" => 0],                                
                                "loops"                 =>  ["instPrf" => 0],
                                "maxdigits"             =>  ["instPrf" => 0],
                                "maxdigits_timeout"     =>  ["instPrf" => 0]
                            ],
    "ticketSla"         =>  [   "idticketsla"           => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0, "tt" => 1],
                                "response_low"          => ["instPrf" => 0],
                                "response_normal"       => ["instPrf" => 0],
                                "response_high"         => ["instPrf" => 0],
                                "solution_low"          => ["instPrf" => 0],
                                "solution_normal"       => ["instPrf" => 0],
                                "solution_high"         => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"]
                            ],
    "crmRecordTypes"    =>  [   "idcrmrecordtype"       => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0],
                                "description"           => ["instPrf" => 0],
                                "deleted"               => ["instPrf" => 0],
                                "created"               => ["instPrf" => 0],    // neuvádět "ti" => 1, jde o tabulku stat. údajů, musejí být k dispozici pro podřazené "crmRecords
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"]
                            ],
    // skupina 3 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "accounts"          =>  [   "idaccount"             => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0, "tt" => 1],
                                "idticketsla"           => ["instPrf" => 1, "fk" => "ticketSla"],
                                "survey"                => ["instPrf" => 0],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "description"           => ["instPrf" => 0],
                                "deleted"               => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "edited"                => ["instPrf" => 0, "ti" => 1],
                                "created"               => ["instPrf" => 0]
                            ],
    "databases"         =>  [   "iddatabase"            => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0, "tt" => 1],
                                "idqueue"               => ["instPrf" => 1, "fk" => "queues"],
                                "iddatabasegroup"       => ["instPrf" => 0, "fk" => "databaseGroups"],  // v IN bucketu má sloupec název "description", PHP science z něj parsuje jen ID databáze
                                "stage"                 => ["instPrf" => 0],
                                "deleted"               => ["instPrf" => 0],
                                "time"                  => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"]
                            ],
    "ticketCategories"  =>  [   "idticketcategory"      => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0, "tt" => 1],
                                "idticketsla"           => ["instPrf" => 1, "fk" => "ticketSla"],
                                "idqueue"               => ["instPrf" => 1, "fk" => "queues"],
                                "survey"                => ["instPrf" => 0],
                                "template_email"        => ["instPrf" => 0],
                                "template_page"         => ["instPrf" => 0],
                                "deleted"               => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"]
                            ],    
    "readySessions"     =>  [   "idreadysession"        =>  ["instPrf" => 1, "pk" => 1],
                                "start_time"            =>  ["instPrf" => 0, "ti" => 1],
                                "end_time"              =>  ["instPrf" => 0],
                                "duration"              =>  ["instPrf" => 0],
                                "iduser"                =>  ["instPrf" => 1, "fk" => "users"]
                            ],
    // skupina 4 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "contacts"          =>  [   "idcontact"             => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0, "tt" => 1],
                                "firstname"             => ["instPrf" => 0],
                                "lastname"              => ["instPrf" => 0],
                                "idaccount"             => ["instPrf" => 1, "fk" => "accounts"],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "description"           => ["instPrf" => 0],
                                "deleted"               => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "form"                  => ["instPrf" => 0, "json" => 1],
                                "number"                => ["instPrf" => 0]
                            ]
];
$tabsInOutV5  = [
    // skupina 3 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "calls"             =>  [   "idcall"                =>  ["instPrf" => 1, "pk" => 1],
                                "call_time"             =>  ["instPrf" => 0, "ti" => 1],
                                "direction"             =>  ["instPrf" => 0],
                                "answered"              =>  ["instPrf" => 0],
                                "idqueue"               =>  ["instPrf" => 1, "fk" => "queues"],
                                "iduser"                =>  ["instPrf" => 1, "fk" => "users"],
                                "iduser_activity"       =>  ["instPrf" => 1],                       // u v5 nebude hodnota nic jiného než '' nebo fakeId -> instPrf může být 0 i 1 (je to jedno)
                                "idrecord"              =>  ["instPrf" => 1, "fk" => "records"],
                                "clid"                  =>  ["instPrf" => 0],
                                "contact"               =>  ["instPrf" => 0],
                                "did"                   =>  ["instPrf" => 0],
                                "wait_time"             =>  ["instPrf" => 0],
                                "ringing_time"          =>  ["instPrf" => 0],
                                "hold_time"             =>  ["instPrf" => 0],
                                "duration"              =>  ["instPrf" => 0],
                                "orig_pos"              =>  ["instPrf" => 0],
                                "position"              =>  ["instPrf" => 0],
                                "disposition_cause"     =>  ["instPrf" => 0],
                                "disconnection_cause"   =>  ["instPrf" => 0],
                                "pressed_key"           =>  ["instPrf" => 0],
                                "missed_call"           =>  ["instPrf" => 0],
                                "missed_call_time"      =>  ["instPrf" => 0],
                                "score"                 =>  ["instPrf" => 0],
                                "note"                  =>  ["instPrf" => 0],
                                "attemps"               =>  ["instPrf" => 0],
                                "qa_user_id"            =>  ["instPrf" => 0],
                                "idinstance"            =>  ["instPrf" => 0, "fk" => "instances"],
                                "idstatus"              =>  ["instPrf" => 1, "fk" => "statuses"],
                                "duration_channels"     =>  ["instPrf" => 0],
                                "transfer_time"         =>  ["instPrf" => 0],
                                "transfer_number"       =>  ["instPrf" => 0],
                                "acw"                   =>  ["instPrf" => 0],
                                "dialer_time"           =>  ["instPrf" => 0],
                                "idactivity"            =>  ["instPrf" => 1, "fk" => "activities"],
                                "activity_name"         =>  ["instPrf" => 0]
                            ] 
];
$tabsInOutV56_part2 = [
    // skupina 3 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "loginSessions"     =>  [   "idloginsession"        =>  ["instPrf" => 1, "pk" => 1],
                                "start_time"            =>  ["instPrf" => 0, "ti" => 1],
                                "end_time"              =>  ["instPrf" => 0],
                                "duration"              =>  ["instPrf" => 0],
                                "iduser"                =>  ["instPrf" => 1, "fk" => "users"]
                            ],
    "pauseSessions"     =>  [   "idpausesession"        =>  ["instPrf" => 1, "pk" => 1],
                                "start_time"            =>  ["instPrf" => 0, "ti" => 1],
                                "end_time"              =>  ["instPrf" => 0],
                                "duration"              =>  ["instPrf" => 0],
                                "idpause"               =>  ["instPrf" => 1, "fk" => "pauses"],
                                "iduser"                =>  ["instPrf" => 1, "fk" => "users"]
                            ],
    "queueSessions"     =>  [   "idqueuesession"        =>  ["instPrf" => 1, "pk" => 1],
                                "start_time"            =>  ["instPrf" => 0, "ti" => 1], 
                                "end_time"              =>  ["instPrf" => 0],
                                "duration"              =>  ["instPrf" => 0],
                                "idqueue"               =>  ["instPrf" => 1, "fk" => "queues"],
                                "iduser"                =>  ["instPrf" => 1, "fk" => "users"]
                            ],
    "idleSessions"      =>  [   "ididlesession"         =>  ["instPrf" => 1, "pk" => 1],
                                "start_time"            =>  ["instPrf" => 0, "ti" => 1], 
                                "end_time"              =>  ["instPrf" => 0],
                                "duration"              =>  ["instPrf" => 0],
                                "iduser"                =>  ["instPrf" => 1, "fk" => "users"]
                            ],
    // skupina 4 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "records"           =>  [   "idrecord"              =>  ["instPrf" => 1, "pk" => 1],
                                "name"                  =>  ["instPrf" => 0],
                                "iduser"                =>  ["instPrf" => 1, "fk" => "users"],
                                "idqueue"               =>  ["instPrf" => 1, "fk" => "queues"],
                                "idstatus"              =>  ["instPrf" => 1, "fk" => "statuses"],
                                "iddatabase"            =>  ["instPrf" => 1, "fk" => "databases"],
                                "number"                =>  ["instPrf" => 0],
                                "idcall"                =>  ["instPrf" => 1, "fk" => "calls"],
                                "action"                =>  ["instPrf" => 0],
                                "edited"                =>  ["instPrf" => 0, "ti" => 1],
                                "created"               =>  ["instPrf" => 0],
                                "nextcall"              =>  ["instPrf" => 0],
                                "idinstance"            =>  ["instPrf" => 0],
                                "form"                  =>  ["instPrf" => 0, "json" => 0]               // "json" => <0/1> ~ jen rozparsovat / rozparsovat a pokračovat ve zpracování hodnoty
                            ],
    // skupina 5 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "recordSnapshots"   =>  [   "idrecordsnapshot"      =>  ["instPrf" => 1, "pk" => 1],
                                "iduser"                =>  ["instPrf" => 1, "fk" => "users"],
                                "idrecord"              =>  ["instPrf" => 1, "fk" => "records"],
                                "idstatus"              =>  ["instPrf" => 1, "fk" => "statuses"],
                                "idcall"                =>  ["instPrf" => 1, "fk" => "calls"],
                                "created"               =>  ["instPrf" => 0, "ti" => 1],
                                "created_by"            =>  ["instPrf" => 1],                           // neuvažujeme jako FK do "users" (není to tak v GD)
                                "nextcall"              =>  ["instPrf" => 0]
                            ]
];
$tabsInOutV6_part2 = [            // vstupně-výstupní tabulky používané pouze u Daktely v6
    // skupina 5 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "tickets"           =>  [   "idticket"              => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0, "tt" => 1],
                                "idticketcategory"      => ["instPrf" => 1, "fk" => "ticketCategories"],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "email"                 => ["instPrf" => 0],
                                "idcontact"             => ["instPrf" => 1, "fk" => "contacts"],
                                "idstatus"              => ["instPrf" => 1, "fk" => "statuses"],
                                "description"           => ["instPrf" => 0],
                                "stage"                 => ["instPrf" => 0],
                                "priority"              => ["instPrf" => 0],
                                "sla_deadtime"          => ["instPrf" => 0],
                                "sla_change"            => ["instPrf" => 0],
                                "sla_notify"            => ["instPrf" => 0],
                                "sla_duration"          => ["instPrf" => 0],
                                "sla_custom"            => ["instPrf" => 0],
                                "survey"                => ["instPrf" => 0],
                                "survey_offered"        => ["instPrf" => 0],
                                "satisfaction"          => ["instPrf" => 0],
                                "satisfaction_comment"  => ["instPrf" => 0],
                                "reopen"                => ["instPrf" => 0],
                                "deleted"               => ["instPrf" => 0],
                                "created"               => ["instPrf" => 0],
                                "edited"                => ["instPrf" => 0],        // neuvádět "ti" => 1, jde o tabulku stat. údajů, musejí být k dispozici pro podřazené "crmRecords"
                                "edited_by"             => ["instPrf" => 1],
                                "first_answer"          => ["instPrf" => 0],
                                "first_answer_duration" => ["instPrf" => 0],
                                "closed"                => ["instPrf" => 0],
                                "unread"                => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "form"                  => ["instPrf" => 0, "json" => 0],
                                "interaction_activity_count" => ["instPrf" => 0],
                                "idmerge"               => ["instPrf" => 1],        // id přidruženého (mergovaného) tiketu
                                "created_by"            => ["instPrf" => 1],
                                "has_attachment"        => ["instPrf" => 0]
                            ],
    // skupina 6 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "crmRecords"        =>  [   "idcrmrecord"           => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],    
                                "title"                 => ["instPrf" => 0, "tt" => 1],
                                "idcrmrecordtype"       => ["instPrf" => 1, "fk" => "crmRecordTypes"],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "idcontact"             => ["instPrf" => 1, "fk" => "contacts"],
                                "idaccount"             => ["instPrf" => 1, "fk" => "accounts"],
                                "idticket"              => ["instPrf" => 1, "fk" => "idticket"],
                                "idstatus"              => ["instPrf" => 1, "fk" => "idstatus"],
                                "description"           => ["instPrf" => 0],
                                "deleted"               => ["instPrf" => 0],
                                "edited"                => ["instPrf" => 0, "ti" => 1],
                                "created"               => ["instPrf" => 0],
                                "stage"                 => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "form"                  => ["instPrf" => 0, "json" => 0]
                            ],
    "activities"        =>  [   "idactivity"            => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0, "tt" => 1],
                                "idcontact"             => ["instPrf" => 1, "fk" => "contacts"],
                                "idticket"              => ["instPrf" => 1, "fk" => "tickets"],
                                "idqueue"               => ["instPrf" => 1, "fk" => "queues"],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "idrecord"              => ["instPrf" => 1, "fk" => "records"],
                                "idstatus"              => ["instPrf" => 1, "fk" => "statuses"],
                                "action"                => ["instPrf" => 0],
                                "type"                  => ["instPrf" => 0],
                                "priority"              => ["instPrf" => 0],
                                "description"           => ["instPrf" => 0],
                                "time"                  => ["instPrf" => 0, "ti" => 1],
                                "time_wait"             => ["instPrf" => 0],
                                "time_open"             => ["instPrf" => 0],
                                "time_close"            => ["instPrf" => 0],
                                "created_by"            => ["instPrf" => 1],
                                "nps_score"             => ["instPrf" => 0],
                                "nps_note"              => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "item"                  => ["instPrf" => 0, "json" => 1]               // "json" => <0/1> ~ jen rozparsovat / rozparsovat a pokračovat ve zpracování hodnoty
                            ],
    // skupina 7 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    "crmRecordSnapshots"=>  [   "idcrmrecordsnapshot"   => ["instPrf" => 1, "pk" => 1],
                                "name"                  => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0, "tt" => 1],
                                "idcontact"             => ["instPrf" => 1, "fk" => "contacts"],
                                "idaccount"             => ["instPrf" => 1, "fk" => "accounts"],
                                "idticket"              => ["instPrf" => 1, "fk" => "tickets"],
                                "idcrmrecord"           => ["instPrf" => 1, "fk" => "crmRecords"],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "idstatus"              => ["instPrf" => 1, "fk" => "statuses"],
                                "idcrmrecordtype"       => ["instPrf" => 1, "fk" => "crmRecordTypes"],
                                "description"           => ["instPrf" => 0],
                                "deleted"               => ["instPrf" => 0],
                                "created_by"            => ["instPrf" => 0],
                                "time"                  => ["instPrf" => 0, "ti" => 1],
                                "stage"                 => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"]
                            ],
    "activitiesAttempt" =>  [   "idactivitiesattempt"   => ["instPrf" => 1, "pk" => 1],
                                "start_time"            => ["instPrf" => 0, "ti" => 1],
                                "end_time"              => ["instPrf" => 0],
                                "duration"              => ["instPrf" => 0],
                                "idcall"                => ["instPrf" => 1, "fk" => "calls"],
                                "idivr"                 => ["instPrf" => 1, "fk" => "ivrs"],
                                "pressed_key"           => ["instPrf" => 0]
                            ],
    "qaReviews"         =>  [   "idqareview"            => ["instPrf" => 1, "pk" => 1],
                                "created"               => ["instPrf" => 0],		
                                "edited"		=> ["instPrf" => 0, "ti" => 1],
                                "created_by"		=> ["instPrf" => 1],
                                "edited_by"		=> ["instPrf" => 1],
                                "qaform_title"		=> ["instPrf" => 0],
                                "idactivity"		=> ["instPrf" => 1, "fk" => "activities"],
                                "rel_score"		=> ["instPrf" => 0],
                                "abs_score"		=> ["instPrf" => 0],
                                "note"                  => ["instPrf" => 0]
                            ]
];
$tabsInOut = [
    5                   =>  array_merge($tabsInOutV56_part1, $tabsInOutV5, $tabsInOutV56_part2),
    6                   =>  array_merge($tabsInOutV56_part1, $tabsInOutV6_part1, $tabsInOutV56_part2, $tabsInOutV6_part2)
];
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// jen výstupní tabulky
$tabsOutOnlyV56 = [         // tabulky, které vytváří transformace a objevují se až na výstupu (nejsou ve vstupním bucketu KBC) používané u Daktely v5 i v6
    "fieldValues"       =>  [   "idfieldvalue"          => ["instPrf" => 1, "pk" => 1],
                                "idrecord"              => ["instPrf" => 1, "fk" => "records"],
                                "idfield"               => ["instPrf" => 1, "fk" => "fields"],
                                "value"                 => ["instPrf" => 0]
                            ],
    "fieldValuesEmpty"  =>  [   "idfieldvalue"          => ["instPrf" => 1, "pk" => 1],             // seznam formulářových polí s prázdnými hodnotami z tabulky "records"
                                "idrecord"              => ["instPrf" => 1, "fk" => "records"],
                                "idfield"               => ["instPrf" => 1, "fk" => "fields"],
                                "key"                   => ["instPrf" => 0]     // systémové označení JSON atributu - název klíče (pro lepší orientaci při ručním porovnávání s JSONy v IN tabulkách
                            ],
    "groups"            =>  [   "idgroup"               => ["instPrf" => 1, "pk" => 1],
                                "title"                 => ["instPrf" => 0, "tt" => 1]
                            ],
    "instances"         =>  [   "idinstance"            => ["instPrf" => 0, "pk" => 1],
                                "url"                   => ["instPrf" => 0]
                            ]
];
$tabsOutOnlyV6 = [          // tabulky, které vytváří transformace a objevují se až na výstupu (nejsou ve vstupním bucketu KBC) používané pouze u Daktely v6
    "databaseGroups"    =>  [   "iddatabasegroup"       => ["instPrf" => 1, "pk" => 1],
                                "title"                 => ["instPrf" => 0, "tt" => 1]
                            ],
    "calls"             =>  [   "idcall"                => ["instPrf" => 1, "pk" => 1],
                                "call_time"             => ["instPrf" => 0],
                                "direction"             => ["instPrf" => 0],
                                "answered"              => ["instPrf" => 0],
                                "idqueue"               => ["instPrf" => 1, "fk" => "queues"],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "iduser_activity"       => ["instPrf" => 1],
                                "idrecord"              => ["instPrf" => 1, "fk" => "records"],
                                "clid"                  => ["instPrf" => 0],
                                "contact"               => ["instPrf" => 0, "fk" => "contacts"],
                                "did"                   => ["instPrf" => 0],
                                "wait_time"             => ["instPrf" => 0],
                                "ringing_time"          => ["instPrf" => 0],
                                "hold_time"             => ["instPrf" => 0],
                                "duration"              => ["instPrf" => 0],
                                "orig_pos"              => ["instPrf" => 0],
                                "position"              => ["instPrf" => 0],
                                "disposition_cause"     => ["instPrf" => 0],
                                "disconnection_cause"   => ["instPrf" => 0],
                                "pressed_key"           => ["instPrf" => 0],
                                "missed_call"           => ["instPrf" => 0],
                                "missed_call_time"      => ["instPrf" => 0],
                                "score"                 => ["instPrf" => 0],
                                "note"                  => ["instPrf" => 0],
                                "attemps"               => ["instPrf" => 0],
                                "qa_user_id"            => ["instPrf" => 0, "fk" => "users"],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "idstatus"              => ["instPrf" => 1, "fk" => "statuses"],
                                "duration_channels"     => ["instPrf" => 0],
                                "transfer_time"         => ["instPrf" => 0],
                                "transfer_number"       => ["instPrf" => 0],
                                "acw"                   => ["instPrf" => 0],
                                "acw_out"               => ["instPrf" => 0],
                                "bcw"                   => ["instPrf" => 0],
                                "dialer_time"           => ["instPrf" => 0],
                                "idactivity"            => ["instPrf" => 1, "fk" => "activities"],
                                "activity_name"         => ["instPrf" => 0]
                            ],    
    "emails"            =>  [   "idemail"               => ["instPrf" => 1, "pk" => 1],
                                "time"                  => ["instPrf" => 0],                 
                                "idqueue"               => ["instPrf" => 1, "fk" => "queues"],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "iduser_activity"       => ["instPrf" => 1],
                                "idrecord"              => ["instPrf" => 1, "fk" => "records"],               
                                "address"               => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "idstatus"              => ["instPrf" => 1, "fk" => "statuses"],
                                "wait_time"             => ["instPrf" => 0],
                                "duration"              => ["instPrf" => 0],
                                "answered"              => ["instPrf" => 0],
                                "direction"             => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0],
                                "idactivity"            => ["instPrf" => 1, "fk" => "activities"],
                                "idticket"              => ["instPrf" => 1, "fk" => "tickets"],
                                //"text"                => ["instPrf" => 0]
                            ],
    "sms"               =>  [   "idsms"                 => ["instPrf" => 1, "pk" => 1],
                                "time"                  => ["instPrf" => 0],                             
                                "idqueue"               => ["instPrf" => 1, "fk" => "queues"],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "iduser_activity"       => ["instPrf" => 1],
                                "idrecord"              => ["instPrf" => 1, "fk" => "records"],              
                                "clid"                  => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "idstatus"              => ["instPrf" => 1, "fk" => "statuses"],
                                "wait_time"             => ["instPrf" => 0],
                                "duration"              => ["instPrf" => 0],
                                "answered"              => ["instPrf" => 0],
                                "direction"             => ["instPrf" => 0],
                                "disconnection"         => ["instPrf" => 0],
                                "idactivity"            => ["instPrf" => 1, "fk" => "activities"],
                                "idticket"              => ["instPrf" => 1, "fk" => "tickets"]
                            ], 
    "chats"             =>  [  "idchat"                 => ["instPrf" => 1, "pk" => 1],
                                "time"                  => ["instPrf" => 0],
                                "idqueue"               => ["instPrf" => 1, "fk" => "queues"],
                                "iduser"                => ["instPrf" => 1, "fk" => "users"],
                                "iduser_activity"       => ["instPrf" => 1],
                                "idrecord"              => ["instPrf" => 1, "fk" => "records"],            
                                "address"               => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "idstatus"              => ["instPrf" => 1, "fk" => "statuses"],
                                "wait_time"             => ["instPrf" => 0],
                                "duration"              => ["instPrf" => 0],
                                "answered"              => ["instPrf" => 0],
                                "title"                 => ["instPrf" => 0],
                                "disconnection"         => ["instPrf" => 0],
                                "nps_score"             => ["instPrf" => 0],
                                "nps_note"              => ["instPrf" => 0],
                                "idactivity"            => ["instPrf" => 1, "fk" => "activities"],
                                "idticket"              => ["instPrf" => 1, "fk" => "tickets"]
                            ],
    "comments"          =>  [   "idcomment"             => ["instPrf" => 1, "pk" => 1],             // idcomment = idactivity (JSON "item" u commentů nic neobsahuje, má hodnotu NULL)
                                "time"                  => ["instPrf" => 0],
                                "idqueue"               => ["instPrf" => 1, "fk" => "queues"],
                                "iduser_activity"       => ["instPrf" => 1],
                                "idrecord"              => ["instPrf" => 1, "fk" => "records"],            
                                "idticket"              => ["instPrf" => 1, "fk" => "tickets"],    
                                "idinstance"            => ["instPrf" => 0, "fk" => "instances"],
                                "idstatus"              => ["instPrf" => 1, "fk" => "statuses"],
                                "description"           => ["instPrf" => 0]
                            ],
    "contFieldVals"     =>  [   "idcontfieldval"        => ["instPrf" => 1, "pk" => 1],
                                "idcontact"             => ["instPrf" => 1, "fk" => "contacts"],
                                "idfield"               => ["instPrf" => 1, "fk" => "fields"],
                                "value"                 => ["instPrf" => 0]
                            ],                                                                      // hodnoty formulářových polí z tabulky "contacts"
    "tickFieldVals"     =>  [   "idtickfieldval"        => ["instPrf" => 1, "pk" => 1],
                                "idticket"              => ["instPrf" => 1, "fk" => "tickets"],
                                "idfield"               => ["instPrf" => 1, "fk" => "fields"],
                                "value"                 => ["instPrf" => 0]
                            ],                                                                      // hodnoty formulářových polí z tabulky "tickets"
    "crmFieldVals"      =>  [   "idcrmfieldval"         => ["instPrf" => 1, "pk" => 1],
                                "idcrmrecord"           => ["instPrf" => 1, "fk" => "crmRecords"],
                                "idfield"               => ["instPrf" => 1, "fk" => "fields"],
                                "value"                 => ["instPrf" => 0]
                            ],                                                                      // hodnoty formulářových polí z tabulky "crmRecords"
    /*
    "contFieldValsEmpty"=>  [   "idcontfieldval"        => ["instPrf" => 1, "pk" => 1],             // seznam formulářových polí s prázdnými hodnotami z tabulky "contacts"
                                "idcontact"             => ["instPrf" => 1, "fk" => "contacts"],
                                "idfield"               => ["instPrf" => 1, "fk" => "fields"],
                                "key"                   => ["instPrf" => 0]     // systémové označení JSON atributu - název klíče (pro lepší orientaci při ručním porovnávání s JSONy v IN tabulkách
                            ],
    "tickFieldValsEmpty"=>  [   "idtickfieldval"        => ["instPrf" => 1, "pk" => 1],             // seznam formulářových polí s prázdnými hodnotami z tabulky "tickets"
                                "idticket"              => ["instPrf" => 1, "fk" => "tickets"],
                                "idfield"               => ["instPrf" => 1, "fk" => "fields"],
                                "key"                   => ["instPrf" => 0]     // systémové označení JSON atributu - název klíče (pro lepší orientaci při ručním porovnávání s JSONy v IN tabulkách
                            ],
    "crmFieldValsEmpty" =>  [   "idcrmfieldval"         => ["instPrf" => 1, "pk" => 1],             // seznam formulářových polí s prázdnými hodnotami z tabulky "crmRecords"
                                "idcrmrecord"           => ["instPrf" => 1, "fk" => "crmRecords"],
                                "idfield"               => ["instPrf" => 1, "fk" => "fields"],
                                "key"                   => ["instPrf" => 0]     // systémové označení JSON atributu - název klíče (pro lepší orientaci při ručním porovnávání s JSONy v IN tabulkách
                            ],
    */ 
    "actItems"          =>  [   "idactitem"             => ["instPrf" => 0, "pk" => 1],
                                "name"                  => ["instPrf" => 0]
                            ],                                                                      // seznam parametrů z pole "item" tabulky "activities"
    "actItemVals"       =>  [   "idactitemval"          => ["instPrf" => 1, "pk" => 1],
                                "idactivity"            => ["instPrf" => 1, "fk" => "activities"],
                                "idactitem"             => ["instPrf" => 1, "fk" => "actItems"],
                                "value"                 => ["instPrf" => 0]
                            ]                                                                       // hodnoty z pole "item" tabulky "activities" 
    
];
$tabsOutOnly = [
    5                   =>  $tabsOutOnlyV56,
    6                   =>  array_merge($tabsOutOnlyV56, $tabsOutOnlyV6)
];
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// parametry parsování JSON řetězců záznamů z formulářových polí do out-only tabulek hodnot formulářových polí
$jsonFieldsOuts = [     // <vstupní tabulka kde se nachází form. pole> => [<název out-only tabulky hodnot form. polí>, <umělý inkrementální index hodnot form. polí>]
    "records"       =>  "fieldValues",
    "contacts"      =>  "contFieldVals",
    "tickets"       =>  "tickFieldVals",
    "crmRecords"    =>  "crmFieldVals",
    "activities"    =>  "actItemVals"
];
// parametry parsování JSON řetězců záznamů z formulářových polí do out-only tabulek seznamu prázdných hodnot formulářových polí (slouží k identifikaci dříve vyplněných a nyní prázdných hodnot)
$jsonFieldsOutsEmpty =[ // <vstupní tabulka kde se nachází form. pole> => [<název out-only tabulky hodnot form. polí>, <umělý inkrementální index hodnot form. polí>]
    "records"       =>  "fieldValuesEmpty",
    /*
    "contacts"      =>  "contFieldValsEmpty",
    "tickets"       =>  "tickFieldValsEmpty",
    "crmRecords"    =>  "crmFieldValsEmpty",
    "activities"    =>  "actItemValsEmpty"
    */
];
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// jen vstupní tabulky
$tabsInOnlyV5  = $tabsInOnlyV56 = [];
$tabsInOnlyV6  = [
    "crmFields"         =>  [   "idcrmfield"            => ["instPrf" => 1],
                                "title"                 => ["instPrf" => 0],
                                "idinstance"            => ["instPrf" => 0],
                                "name"                  => ["instPrf" => 0]
                            ]    
];
$tabsInOnly = [
    5                   =>  array_merge($tabsInOnlyV5, $tabsInOnlyV56),
    6                   =>  array_merge($tabsInOnlyV56, $tabsInOnlyV6)
];
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// jen vstupní sloupce
$colsInOnly = [         // seznam sloupců, které se nepropíší do výstupních tabulek (slouží jen k internímu zpracování)
 // "název_tabulky"     =>  ["název_sloupce_1", "název_sloupce_2, ...]
    "fields"            =>  ["name"],   // systémové názvy formulářových polí, slouží jen ke spárování "čitelných" názvů polí s hodnotami polí parsovanými z JSONu
    "records"           =>  ["form"],   // hodnoty formulářových polí z tabulky "records"    jako neparsovaný JSON
    "contacts"          =>  ["form"],   // hodnoty formulářových polí z tabulky "contacts"   jako neparsovaný JSON
    "tickets"           =>  ["form"],   // hodnoty formulářových polí z tabulky "tickets"    jako neparsovaný JSON
    "crmRecords"        =>  ["form"],   // hodnoty formulářových polí z tabulky "crmRecords" jako neparsovaný JSON
    //"activities"      =>  [...]
];
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// proměnné pro práci se všemi tabulkami
$tabsList_InOut = [
    5                   =>  array_keys($tabsInOut[5]),
    6                   =>  array_keys($tabsInOut[6])
];
$tabs_InOut_InOnly = [  // nutno dodržet pořadí spojování polí, aby in-only tabulka "crmFields" (v6) byla před tabulkami závislými na "fields" !
    5                   => array_merge($tabsInOnly[5], $tabsInOut[5]),
    6                   => array_merge($tabsInOnly[6], $tabsInOut[6])
];
$tabs_InOut_OutOnly = [      
    5                   => array_merge($tabsInOut[5], $tabsOutOnly[5]),
    6                   => array_merge($tabsInOut[6], $tabsOutOnly[6])
];
$tabsList_InOut_InOnly = [
    5                   => array_keys($tabs_InOut_InOnly[5]),
    6                   => array_keys($tabs_InOut_InOnly[6])
];
$tabsList_InOut_OutOnly = [
    5                   => array_keys($tabs_InOut_OutOnly[5]),
    6                   => array_keys($tabs_InOut_OutOnly[6])
];
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// pole obsahující unikátní seznam výstupních tabulek všech verzí Daktely s počtem sloupců jednotlivých tabulek
$outTabsColsCount = [];
foreach ($tabs_InOut_OutOnly as $verTabs) {                     // iterace podle verzí Daktely (klíč = 5, 6, ...)
    foreach ($verTabs as $tab => $cols) {                       // iterace definic tabulek v rámci dané verze
        $colNames = array_key_exists($tab, $colsInOnly) ? array_diff(array_keys($cols), $colsInOnly[$tab]) : array_keys($cols); // jsou-li některé sloupce jen vstupní nezapočtou se
        if (!array_key_exists($tab, $outTabsColsCount)) {
            $outTabsColsCount[$tab] = count($colNames);
        }
    }
}
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// seznam výstupních tabulek, u kterých požadujeme mít ID a hodnoty společné pro všechny instance
                // "název_tabulky" => 0/1 ~ vypnutí/zapnutí volitelného požadavku na indexaci záznamů v tabulce společnou pro všechny instance
$instCommonOuts = ["statuses" => 0, "groups" => 1, "databaseGroups" => 1];
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ostatní proměnné

// volitelná náhrada prázdných hodnot ID umělou hodnotou ID, která odpovídá umělému title
// motivace:  pro joinování tabulek v GD (tam se prázdná hodnota defaultně označuje jako "(empty value)")
$emptyToNA   = true;
$fakeId      = "0";
$fakeTitle   = "";                                          // původně "(empty value)"
$tabsFakeRow = $tabsList_InOut_OutOnly[6] + $tabsList_InOut_OutOnly[5];
                                                            // = všechny InOut tabulky napříč verzemi (původně jen ["users", "statuses"])
                                                            // sjednocení polí je nekomutativní!! - jinak jsou ve výsledném poli názvy některých tabulek 2× !!

// počty číslic, na které jsou doplňovány ID's (kvůli řazení v GoodData je výhodné mít konst. délku ID's) a oddělovač prefixu od hodnoty
$idFormat = [
    "sep"       =>  "",                                     // znak oddělující ID instance od inkrementálního ID dané tabulky ("", "-" apod.)
    //"instId"  =>  ceil(log10(max(2, count($instances)))), // počet číslic, na které je doplňováno ID instance (hodnota před oddělovačem) - určuje se dle počtu instancí
    "instId"    =>  1,                                      // počet číslic, na které je doplňováno ID instance (hodnota před oddělovačem) - určuje se dle počtu instancí
    "idTab"     =>  8,                                      // výchozí počet číslic, na které je doplňováno inkrementální ID dané tabulky (hodnota za oddělovačem);
                                                            // příznakem potvrzujícím, že hodnota dostačovala k indexaci záznamů u všech tabulek, je proměnná $idFormatIdEnoughDigits;
                                                            // nedoplňovat = "" / 0 / NULL / []  (~ hodnota, kterou lze vyhodnotit jako empty)    
    "idField"   =>  3                                       // výchozí počet číslic, na které je doplňováno inkrementální ID hodnot konkrétního form. pole
];

// delimitery názvu skupiny v queues.idgroup
$delim = [ "L" => "[[" , "R" => "]]" ];

// systémové řetězce určené pro řízení custom skriptů Daktely v atributech "description" apod., které mají být v BI ignorovány;
// tvar pole:  [ "[[str_1]]", "[[str_2]]", "[[str_3]]" ]
$descriptParseIgnoredStrings = [ "[[CLEANPASTCALLS]]" ];

// defaultní počet znaků pro ořez řetězců hodnot z JSONů (hodnoty form. polí, hodnoty z activities.items)
$strTrimDefaultLen = 3980;                                  // GD dovolí až 65 535 znaků, writer do GD/SSRS max. 4000 znaků (nvarchar(4000) 
                                                            // -> rezerva na připojený text " ... (zkráceno) => zvolen limit délky řetězce 3980 znaků;
                                                            // někde (např. u activities.items) se vyskytují i delší řetězce!  

// max. uvažovaná délka tel. čísla v mezinár. tvaru (pro fci "isValidPhoneNumber")
$phoneNumMaxLen = 20;   // max. 20 číslic

// proměnná "action" typu ENUM u campaignRecords - převodní pole číselných kódů akcí na názvy akcí
$campRecordsActions = [
    "0" => "Not assigned",
    "1" => "Ready",
    "2" => "Called",
    "3" => "Call in progress",
    "4" => "Hangup",
    "5" => "Done",
    "6" => "Rescheduled"
];

// klíčová slova pro identifikaci typů formulářových polí a pro validaci + konverzi obsahu formulářových polí
$keywords = [
    "dateEq" => ["od", "do"],
    "mailEq" => ["mail", "email", "e-mail"],
    "date"   => ["datum"],
    "name"   => ["jméno", "jmeno", "příjmení", "prijmeni", "řidič", "ceo", "makléř", "předseda"],
    "addr"   => ["adresa", "address", "město", "mesto", "obec", "část obce", "ulice", "čtvrť", "ctvrt", "okres"],
    "psc"    => ["psč", "psc"],
    "addrVal"=> ["do","k","ke","mezi","na","nad","pod","před","při","pri","u","ve","za","čtvrť","ctvrt","sídliště","sidliste","sídl.","sidl.",
                 "ulice","ul.","třída","trida","tř.","tr.","nábřeží","nábř.","nabrezi","nabr.","alej","sady","park","provincie","svaz","území","uzemi",
                 "království","kralovstvi","republika","stát","stat","ostrovy", "okr.","okres","kraj", "kolonie","č.o.","c.o.","č.p.","c.p."],
                 // místopisné předložky a označení
    "romnVal"=> ["i", "ii", "iii", "iv", "vi", "vii", "viii", "ix", "x", "xi", "xii", "xiii", "xiv", "xv", "xvi", "xvii", "xviii", "xix", "xx"],
    "noConv" => ["v"]   // nelze rozhodnout mezi místopis. předložkou a řím. číslem → nekonvertovat case    
];

// regulární výrazy
// hranaté závorky a jejich obsah - použití v preg_replace např. u aktivit typu comment
$regexSquareBracketsAndContent = "/\[.*\]/";
// identifikace jiných znaků než písmen (povolena diakritika), číslic a vyjmenovaných spec. znaků - použití v preg_replace např. u aktivit typu comment
//$regexAlnumAndSpecCharsOnly = "/[^a-zA-Z0-9À-ž \"`\'#%&@€°®©ß%§~,:;<>=_\.\^\?!\(\)\[\]\{\}\|\$\+\-\\*\\\\\/\x0A]/";

// předpis pro konverzi UTF-8 znaků na znaky bez diakritiky (použití např. u těl e-mailů)
$accentsConversion = [
    'À'=>'A','Á'=>'A','Â'=>'A','Ã'=>'A','Ä'=>'A','Å'=>'A','Ç'=>'C','È'=>'E','É'=>'E','Ê'=>'E','Ë'=>'E','Ì'=>'I','Í'=>'I','Î'=>'I','Ï'=>'I','Ñ'=>'N',
    'Ò'=>'O','Ó'=>'O','Ô'=>'O','Õ'=>'O','Ö'=>'O','Ø'=>'O','Ù'=>'U','Ú'=>'U','Û'=>'U','Ü'=>'U','Ý'=>'Y','à'=>'a','á'=>'a','â'=>'a','ã'=>'a','ä'=>'a',
    'å'=>'a','ç'=>'c','è'=>'e','é'=>'e','ê'=>'e','ë'=>'e','ì'=>'i','í'=>'i','î'=>'i','ï'=>'i','ñ'=>'n','ò'=>'o','ó'=>'o','ô'=>'o','õ'=>'o','ö'=>'o',
    'ø'=>'o','ù'=>'u','ú'=>'u','û'=>'u','ü'=>'u','ý'=>'y','ÿ'=>'y','Ā'=>'A','ā'=>'a','Ă'=>'A','ă'=>'a','Ą'=>'A','ą'=>'a','Ć'=>'C','ć'=>'c','Ĉ'=>'C',
    'ĉ'=>'c','Ċ'=>'C','ċ'=>'c','Č'=>'C','č'=>'c','Ď'=>'D','ď'=>'d','Đ'=>'D','đ'=>'d','Ē'=>'E','ē'=>'e','Ĕ'=>'E','ĕ'=>'e','Ė'=>'E','ė'=>'e','Ę'=>'E',
    'ę'=>'e','Ě'=>'E','ě'=>'e','Ĝ'=>'G','ĝ'=>'g','Ğ'=>'G','ğ'=>'g','Ġ'=>'G','ġ'=>'g','Ģ'=>'G','ģ'=>'g','Ĥ'=>'H','ĥ'=>'h','Ħ'=>'H','ħ'=>'h','Ĩ'=>'I',
    'ĩ'=>'i','Ī'=>'I','ī'=>'i','Ĭ'=>'I','ĭ'=>'i','Į'=>'I','į'=>'i','İ'=>'I','ı'=>'i','Ĵ'=>'J','ĵ'=>'j','Ķ'=>'K','ķ'=>'k','Ĺ'=>'L','ĺ'=>'l','Ļ'=>'L',
    'ļ'=>'l','Ľ'=>'L','ľ'=>'l','Ŀ'=>'L','ŀ'=>'l','Ł'=>'L','ł'=>'l','Ń'=>'N','ń'=>'n','Ņ'=>'N','ņ'=>'n','Ň'=>'N','ň'=>'n','ŉ'=>'n','Ō'=>'O','ō'=>'o',
    'Ŏ'=>'O','ŏ'=>'o','Ő'=>'O','ő'=>'o','Ŕ'=>'R','ŕ'=>'r','Ŗ'=>'R','ŗ'=>'r','Ř'=>'R','ř'=>'r','Ś'=>'S','ś'=>'s','Ŝ'=>'S','ŝ'=>'s','Ş'=>'S','ş'=>'s',
    'Š'=>'S','š'=>'s','Ţ'=>'T','ţ'=>'t','Ť'=>'T','ť'=>'t','Ŧ'=>'T','ŧ'=>'t','Ũ'=>'U','ũ'=>'u','Ū'=>'U','ū'=>'u','Ŭ'=>'U','ŭ'=>'u','Ů'=>'U','ů'=>'u',
    'Ű'=>'U','ű'=>'u','Ų'=>'U','ų'=>'u','Ŵ'=>'W','ŵ'=>'w','Ŷ'=>'Y','ŷ'=>'y','Ÿ'=>'Y','Ź'=>'Z','ź'=>'z','Ż'=>'Z','ż'=>'z','Ž'=>'Z','ž'=>'z','ƀ'=>'b',
    'Ɓ'=>'B','Ƃ'=>'B','ƃ'=>'b','Ƈ'=>'C','ƈ'=>'c','Ɗ'=>'D','Ƌ'=>'D','ƌ'=>'d','Ƒ'=>'F','ƒ'=>'f','Ɠ'=>'G','Ɨ'=>'I','Ƙ'=>'K','ƙ'=>'k','ƚ'=>'l','Ɲ'=>'N',
    'ƞ'=>'n','Ɵ'=>'O','Ơ'=>'O','ơ'=>'o','Ƥ'=>'P','ƥ'=>'p','ƫ'=>'t','Ƭ'=>'T','ƭ'=>'t','Ʈ'=>'T','Ư'=>'U','ư'=>'u','Ʋ'=>'V','Ƴ'=>'Y','ƴ'=>'y','Ƶ'=>'Z',
    'ƶ'=>'z','ǅ'=>'D','ǈ'=>'L','ǋ'=>'N','Ǎ'=>'A','ǎ'=>'a','Ǐ'=>'I','ǐ'=>'i','Ǒ'=>'O','ǒ'=>'o','Ǔ'=>'U','ǔ'=>'u','Ǖ'=>'U','ǖ'=>'u','Ǘ'=>'U','ǘ'=>'u',
    'Ǚ'=>'U','ǚ'=>'u','Ǜ'=>'U','ǜ'=>'u','Ǟ'=>'A','ǟ'=>'a','Ǡ'=>'A','ǡ'=>'a','Ǥ'=>'G','ǥ'=>'g','Ǧ'=>'G','ǧ'=>'g','Ǩ'=>'K','ǩ'=>'k','Ǫ'=>'O','ǫ'=>'o',
    'Ǭ'=>'O','ǭ'=>'o','ǰ'=>'j','ǲ'=>'D','Ǵ'=>'G','ǵ'=>'g','Ǹ'=>'N','ǹ'=>'n','Ǻ'=>'A','ǻ'=>'a','Ǿ'=>'O','ǿ'=>'o','Ȁ'=>'A','ȁ'=>'a','Ȃ'=>'A','ȃ'=>'a',
    'Ȅ'=>'E','ȅ'=>'e','Ȇ'=>'E','ȇ'=>'e','Ȉ'=>'I','ȉ'=>'i','Ȋ'=>'I','ȋ'=>'i','Ȍ'=>'O','ȍ'=>'o','Ȏ'=>'O','ȏ'=>'o','Ȑ'=>'R','ȑ'=>'r','Ȓ'=>'R','ȓ'=>'r',
    'Ȕ'=>'U','ȕ'=>'u','Ȗ'=>'U','ȗ'=>'u','Ș'=>'S','ș'=>'s','Ț'=>'T','ț'=>'t','Ȟ'=>'H','ȟ'=>'h','Ƞ'=>'N','ȡ'=>'d','Ȥ'=>'Z','ȥ'=>'z','Ȧ'=>'A','ȧ'=>'a',
    'Ȩ'=>'E','ȩ'=>'e','Ȫ'=>'O','ȫ'=>'o','Ȭ'=>'O','ȭ'=>'o','Ȯ'=>'O','ȯ'=>'o','Ȱ'=>'O','ȱ'=>'o','Ȳ'=>'Y','ȳ'=>'y','ȴ'=>'l','ȵ'=>'n','ȶ'=>'t','ȷ'=>'j',
    'Ⱥ'=>'A','Ȼ'=>'C','ȼ'=>'c','Ƚ'=>'L','Ⱦ'=>'T','ȿ'=>'s','ɀ'=>'z','Ƀ'=>'B','Ʉ'=>'U','Ɇ'=>'E','ɇ'=>'e','Ɉ'=>'J','ɉ'=>'j','ɋ'=>'q','Ɍ'=>'R','ɍ'=>'r',
    'Ɏ'=>'Y','ɏ'=>'y','ɓ'=>'b','ɕ'=>'c','ɖ'=>'d','ɗ'=>'d','ɟ'=>'j','ɠ'=>'g','ɦ'=>'h','ɨ'=>'i','ɫ'=>'l','ɬ'=>'l','ɭ'=>'l','ɱ'=>'m','ɲ'=>'n','ɳ'=>'n',
    'ɵ'=>'o','ɼ'=>'r','ɽ'=>'r','ɾ'=>'r','ʂ'=>'s','ʄ'=>'j','ʈ'=>'t','ʉ'=>'u','ʋ'=>'v','ʐ'=>'z','ʑ'=>'z','ʝ'=>'j','ʠ'=>'q','ͣ'=>'a','ͤ'=>'e','ͥ'=>'i',
    'ͦ'=>'o','ͧ'=>'u','ͨ'=>'c','ͩ'=>'d','ͪ'=>'h','ͫ'=>'m','ͬ'=>'r','ͭ'=>'t','ͮ'=>'v','ͯ'=>'x','ᵢ'=>'i','ᵣ'=>'r','ᵤ'=>'u','ᵥ'=>'v','ᵬ'=>'b','ᵭ'=>'d',
    'ᵮ'=>'f','ᵯ'=>'m','ᵰ'=>'n','ᵱ'=>'p','ᵲ'=>'r','ᵳ'=>'r','ᵴ'=>'s','ᵵ'=>'t','ᵶ'=>'z','ᵻ'=>'i','ᵽ'=>'p','ᵾ'=>'u','ᶀ'=>'b','ᶁ'=>'d','ᶂ'=>'f','ᶃ'=>'g',
    'ᶄ'=>'k','ᶅ'=>'l','ᶆ'=>'m','ᶇ'=>'n','ᶈ'=>'p','ᶉ'=>'r','ᶊ'=>'s','ᶌ'=>'v','ᶍ'=>'x','ᶎ'=>'z','ᶏ'=>'a','ᶑ'=>'d','ᶒ'=>'e','ᶖ'=>'i','ᶙ'=>'u','᷊'=>'r',
    'ᷗ'=>'c','ᷚ'=>'g','ᷜ'=>'k','ᷝ'=>'l','ᷠ'=>'n','ᷣ'=>'r','ᷤ'=>'s','ᷦ'=>'z','Ḁ'=>'A','ḁ'=>'a','Ḃ'=>'B','ḃ'=>'b','Ḅ'=>'B','ḅ'=>'b','Ḇ'=>'B','ḇ'=>'b',
    'Ḉ'=>'C','ḉ'=>'c','Ḋ'=>'D','ḋ'=>'d','Ḍ'=>'D','ḍ'=>'d','Ḏ'=>'D','ḏ'=>'d','Ḑ'=>'D','ḑ'=>'d','Ḓ'=>'D','ḓ'=>'d','Ḕ'=>'E','ḕ'=>'e','Ḗ'=>'E','ḗ'=>'e',
    'Ḙ'=>'E','ḙ'=>'e','Ḛ'=>'E','ḛ'=>'e','Ḝ'=>'E','ḝ'=>'e','Ḟ'=>'F','ḟ'=>'f','Ḡ'=>'G','ḡ'=>'g','Ḣ'=>'H','ḣ'=>'h','Ḥ'=>'H','ḥ'=>'h','Ḧ'=>'H','ḧ'=>'h',
    'Ḩ'=>'H','ḩ'=>'h','Ḫ'=>'H','ḫ'=>'h','Ḭ'=>'I','ḭ'=>'i','Ḯ'=>'I','ḯ'=>'i','Ḱ'=>'K','ḱ'=>'k','Ḳ'=>'K','ḳ'=>'k','Ḵ'=>'K','ḵ'=>'k','Ḷ'=>'L','ḷ'=>'l',
    'Ḹ'=>'L','ḹ'=>'l','Ḻ'=>'L','ḻ'=>'l','Ḽ'=>'L','ḽ'=>'l','Ḿ'=>'M','ḿ'=>'m','Ṁ'=>'M','ṁ'=>'m','Ṃ'=>'M','ṃ'=>'m','Ṅ'=>'N','ṅ'=>'n','Ṇ'=>'N','ṇ'=>'n',
    'Ṉ'=>'N','ṉ'=>'n','Ṋ'=>'N','ṋ'=>'n','Ṍ'=>'O','ṍ'=>'o','Ṏ'=>'O','ṏ'=>'o','Ṑ'=>'O','ṑ'=>'o','Ṓ'=>'O','ṓ'=>'o','Ṕ'=>'P','ṕ'=>'p','Ṗ'=>'P','ṗ'=>'p',
    'Ṙ'=>'R','ṙ'=>'r','Ṛ'=>'R','ṛ'=>'r','Ṝ'=>'R','ṝ'=>'r','Ṟ'=>'R','ṟ'=>'r','Ṡ'=>'S','ṡ'=>'s','Ṣ'=>'S','ṣ'=>'s','Ṥ'=>'S','ṥ'=>'s','Ṧ'=>'S','ṧ'=>'s',
    'Ṩ'=>'S','ṩ'=>'s','Ṫ'=>'T','ṫ'=>'t','Ṭ'=>'T','ṭ'=>'t','Ṯ'=>'T','ṯ'=>'t','Ṱ'=>'T','ṱ'=>'t','Ṳ'=>'U','ṳ'=>'u','Ṵ'=>'U','ṵ'=>'u','Ṷ'=>'U','ṷ'=>'u',
    'Ṹ'=>'U','ṹ'=>'u','Ṻ'=>'U','ṻ'=>'u','Ṽ'=>'V','ṽ'=>'v','Ṿ'=>'V','ṿ'=>'v','Ẁ'=>'W','ẁ'=>'w','Ẃ'=>'W','ẃ'=>'w','Ẅ'=>'W','ẅ'=>'w','Ẇ'=>'W','ẇ'=>'w',
    'Ẉ'=>'W','ẉ'=>'w','Ẋ'=>'X','ẋ'=>'x','Ẍ'=>'X','ẍ'=>'x','Ẏ'=>'Y','ẏ'=>'y','Ẑ'=>'Z','ẑ'=>'z','Ẓ'=>'Z','ẓ'=>'z','Ẕ'=>'Z','ẕ'=>'z','ẖ'=>'h','ẗ'=>'t',
    'ẘ'=>'w','ẙ'=>'y','ẚ'=>'a','Ạ'=>'A','ạ'=>'a','Ả'=>'A','ả'=>'a','Ấ'=>'A','ấ'=>'a','Ầ'=>'A','ầ'=>'a','Ẩ'=>'A','ẩ'=>'a','Ẫ'=>'A','ẫ'=>'a','Ậ'=>'A',
    'ậ'=>'a','Ắ'=>'A','ắ'=>'a','Ằ'=>'A','ằ'=>'a','Ẳ'=>'A','ẳ'=>'a','Ẵ'=>'A','ẵ'=>'a','Ặ'=>'A','ặ'=>'a','Ẹ'=>'E','ẹ'=>'e','Ẻ'=>'E','ẻ'=>'e','Ẽ'=>'E',
    'ẽ'=>'e','Ế'=>'E','ế'=>'e','Ề'=>'E','ề'=>'e','Ể'=>'E','ể'=>'e','Ễ'=>'E','ễ'=>'e','Ệ'=>'E','ệ'=>'e','Ỉ'=>'I','ỉ'=>'i','Ị'=>'I','ị'=>'i','Ọ'=>'O',
    'ọ'=>'o','Ỏ'=>'O','ỏ'=>'o','Ố'=>'O','ố'=>'o','Ồ'=>'O','ồ'=>'o','Ổ'=>'O','ổ'=>'o','Ỗ'=>'O','ỗ'=>'o','Ộ'=>'O','ộ'=>'o','Ớ'=>'O','ớ'=>'o','Ờ'=>'O',
    'ờ'=>'o','Ở'=>'O','ở'=>'o','Ỡ'=>'O','ỡ'=>'o','Ợ'=>'O','ợ'=>'o','Ụ'=>'U','ụ'=>'u','Ủ'=>'U','ủ'=>'u','Ứ'=>'U','ứ'=>'u','Ừ'=>'U','ừ'=>'u','Ử'=>'U',
    'ử'=>'u','Ữ'=>'U','ữ'=>'u','Ự'=>'U','ự'=>'u','Ỳ'=>'Y','ỳ'=>'y','Ỵ'=>'Y','ỵ'=>'y','Ỷ'=>'Y','ỷ'=>'y','Ỹ'=>'Y','ỹ'=>'y','Ỿ'=>'Y','ỿ'=>'y','ⁱ'=>'i',
    'ⁿ'=>'n','ₐ'=>'a','ₑ'=>'e','ₒ'=>'o','ₓ'=>'x','⒜'=>'a','⒝'=>'b','⒞'=>'c','⒟'=>'d','⒠'=>'e','⒡'=>'f','⒢'=>'g','⒣'=>'h','⒤'=>'i','⒥'=>'j','⒦'=>'k',
    '⒧'=>'l','⒨'=>'m','⒩'=>'n','⒪'=>'o','⒫'=>'p','⒬'=>'q','⒭'=>'r','⒮'=>'s','⒯'=>'t','⒰'=>'u','⒱'=>'v','⒲'=>'w','⒳'=>'x','⒴'=>'y','⒵'=>'z','Ⓐ'=>'A',
    'Ⓑ'=>'B','Ⓒ'=>'C','Ⓓ'=>'D','Ⓔ'=>'E','Ⓕ'=>'F','Ⓖ'=>'G','Ⓗ'=>'H','Ⓘ'=>'I','Ⓙ'=>'J','Ⓚ'=>'K','Ⓛ'=>'L','Ⓜ'=>'M','Ⓝ'=>'N','Ⓞ'=>'O','Ⓟ'=>'P','Ⓠ'=>'Q',
    'Ⓡ'=>'R','Ⓢ'=>'S','Ⓣ'=>'T','Ⓤ'=>'U','Ⓥ'=>'V','Ⓦ'=>'W','Ⓧ'=>'X','Ⓨ'=>'Y','Ⓩ'=>'Z','ⓐ'=>'a','ⓑ'=>'b','ⓒ'=>'c','ⓓ'=>'d','ⓔ'=>'e','ⓕ'=>'f','ⓖ'=>'g',
    'ⓗ'=>'h','ⓘ'=>'i','ⓙ'=>'j','ⓚ'=>'k','ⓛ'=>'l','ⓜ'=>'m','ⓝ'=>'n','ⓞ'=>'o','ⓟ'=>'p','ⓠ'=>'q','ⓡ'=>'r','ⓢ'=>'s','ⓣ'=>'t','ⓤ'=>'u','ⓥ'=>'v','ⓦ'=>'w',
    'ⓧ'=>'x','ⓨ'=>'y','ⓩ'=>'z','Ⱡ'=>'L','ⱡ'=>'l','Ɫ'=>'L','Ᵽ'=>'P','Ɽ'=>'R','ⱥ'=>'a','ⱦ'=>'t','Ⱨ'=>'H','ⱨ'=>'h','Ⱪ'=>'K','ⱪ'=>'k','Ⱬ'=>'Z','ⱬ'=>'z',
    'Ɱ'=>'M','ⱱ'=>'v','Ⱳ'=>'W','ⱳ'=>'w','ⱴ'=>'v','ⱸ'=>'e','ⱺ'=>'o','ⱼ'=>'j','Ꝁ'=>'K','ꝁ'=>'k','Ꝃ'=>'K','ꝃ'=>'k','Ꝅ'=>'K','ꝅ'=>'k','Ꝉ'=>'L','ꝉ'=>'l',
    'Ꝋ'=>'O','ꝋ'=>'o','Ꝍ'=>'O','ꝍ'=>'o','Ꝑ'=>'P','ꝑ'=>'p','Ꝓ'=>'P','ꝓ'=>'p','Ꝕ'=>'P','ꝕ'=>'p','Ꝗ'=>'Q','ꝗ'=>'q','Ꝙ'=>'Q','ꝙ'=>'q','Ꝛ'=>'R','ꝛ'=>'r',
    'Ꝟ'=>'V','ꝟ'=>'v','Ａ'=>'A','Ｂ'=>'B','Ｃ'=>'C','Ｄ'=>'D','Ｅ'=>'E','Ｆ'=>'F','Ｇ'=>'G','Ｈ'=>'H','Ｉ'=>'I','Ｊ'=>'J','Ｋ'=>'K','Ｌ'=>'L','Ｍ'=>'M','Ｎ'=>'N',
    'Ｏ'=>'O','Ｐ'=>'P','Ｑ'=>'Q','Ｒ'=>'R','Ｓ'=>'S','Ｔ'=>'T','Ｕ'=>'U','Ｖ'=>'V','Ｗ'=>'W','Ｘ'=>'X','Ｙ'=>'Y','Ｚ'=>'Z','ａ'=>'a','ｂ'=>'b','ｃ'=>'c','ｄ'=>'d',
    'ｅ'=>'e','ｆ'=>'f','ｇ'=>'g','ｈ'=>'h','ｉ'=>'i','ｊ'=>'j','ｋ'=>'k','ｌ'=>'l','ｍ'=>'m','ｎ'=>'n','ｏ'=>'o','ｐ'=>'p','ｑ'=>'q','ｒ'=>'r','ｓ'=>'s','ｔ'=>'t',
    'ｕ'=>'u','ｖ'=>'v','ｗ'=>'w','ｘ'=>'x','ｙ'=>'y','ｚ'=>'z'
];