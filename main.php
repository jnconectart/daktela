<?php
// TRANSFORMACE DAT Z LIBOVOLNÉHO POČTU INSTANCÍ DAKTELA

require_once "vendor/autoload.php";

$ds      = DIRECTORY_SEPARATOR;
$dataDir = getenv("KBC_DATADIR");
$homeDir = __DIR__;

require_once $homeDir.$ds."kbcparams.php";                                      // načtení parametrů importovaných z konfiguračního JSON řetězce v definici PHP aplikace v KBC
require_once $homeDir.$ds."variables.php";                                      // načtení definic proměnných a konstant
require_once $homeDir.$ds."functions.php";                                      // načtení definic funkcí

error_reporting(0);                                                             // vypnutí výpisu chybových hlášek (např. kvůli 'deprecated errors' u PHP 8.1)

logInfo("PROMĚNNÉ A FUNKCE ZAVEDENY");                                          // volitelný diagnostický výstup do logu
logInfo("ZPRACOVÁVANÝ DATUMOVÝ ROZSAH:  ".$processedDates["start"]." ÷ ".$processedDates["end"]);
// ==============================================================================================================================================================================================
// načtení vstupních souborů
foreach ($instances as $instId => $inst) {
    foreach ($tabsList_InOut_InOnly[$inst["ver"]] as $file) {
        ${"in_".$file."_".$instId} = new Keboola\Csv\CsvFile($dataDir."in".$ds."tables".$ds."in_".$file."_".$instId.".csv");
    }
}
logInfo("VSTUPNÍ SOUBORY NAČTENY");     // volitelný diagnostický výstup do logu
// ==============================================================================================================================================================================================
logInfo("ZAHÁJENO NAČÍTÁNÍ DEFINICE DATOVÉHO MODELU");                          // volitelný diagnostický výstup do logu
$jsonList = $tiList = $fkList = $dumpColList = [];
/* struktura polí:  $jsonList    = [$instId => [$tab => [$colName => <0~jen rozparsovat / 1~rozparsovat a pokračovat ve zpracování hodnoty>]]]  ... pole sloupců obsahujících JSON                    
                    $tiList      = [$instId => [$tab => <ID_časového_atributu>]]                                                                ... pole indexů sloupců pro časovou restrikci záznamů
                    $fkList      = [$instId => [$tab => [$colName => <název_nadřazené_tabulky>]]]                                               ... pole názvů nadřazených tabulek pro každý sloupec, který je FK
                    $dumpColList = [$instId => [$tab => 
                                    "dumpColId" => $dumpColId, "dumpColName" => $dumpColName, "dumpColVals" => [$dumpColVal1,...,$dumpColValN]]]... pole tabulek, sloupců a jejich hodnot, které mají být dumpovány
*/
foreach ($instances as $instId => $inst) {                                      // iterace instancí
    logInfo("NAČÍTÁNÍ DEFINICE INSTANCE ".$instId);                             // volitelný diagnostický výstup do logu        
    
    foreach ($tabs_InOut_InOnly[$inst["ver"]] as $tab => $cols) {               // iterace tabulek; $tab - název tabulky, $cols - pole s parametry sloupců
        logInfo("NAČÍTÁNÍ DEFINICE TABULKY ".$instId."_".$tab);                 // volitelný diagnostický výstup do logu   
        
        $colId   = 0;                                                           // počitadlo sloupců (číslováno od 0)
        $tiColId = NULL;                                                        // ID sloupce, který je v dané tabulce atributem pro datumovou restrikci (číslováno od 0)
        foreach ($cols as $colName => $colAttrs) {                              // iterace sloupců
            if (array_key_exists("json", $colAttrs)) {                          // nalezen sloupec, který je JSON
                $jsonList[$instId][$tab][$colName] = $colAttrs["json"];         // uložení příznaku způsobu zpracování JSONu (0/1) do pole $jsonList                                         //
                logInfo("TABULKA ".$instId."_".$tab." - NALEZEN JSON ".$colName."; DALŠÍ ZPRACOVÁNÍ PO PARSOVÁNÍ = ".$colAttrs["json"]);
            }
            if (array_key_exists("ti", $colAttrs)) {                            // nalezen sloupec, který je atributem pro časovou restrikci záznamů
                $tiColId = $colId;
                $tiList[$instId][$tab] = $colId;                                // uložení indexu sloupce (0, 1, 2, ...) do pole $tiList                                         //
                logInfo("TABULKA ".$instId."_".$tab." - ATRIBUT PRO ČASOVOU RESTRIKCI ZÁZNAMŮ: SLOUPEC #".$colId." (".$colName.")");
            }
            if (array_key_exists("fk", $colAttrs)) {                            // nalezen sloupec, který je PK
                $fkList[$instId][$tab][$colName] = $colAttrs["fk"];             // uložení názvu nadřezené tabulky do pole $fkList
                logInfo("TABULKA ".$instId."_".$tab." - NALEZEN FK DO TABULKY ".$colAttrs["fk"]." (SLOUPEC ".$colName.")");
            }
            if ($adhocDump["active"]) {
                if (arrVal(["needles", (string)$instId, $tab, "dumpColName"], $adhocDump) == $colName) {    // je požadován dump z aktuální tabulky
                    logInfo("TABULKA ".$instId."_".$tab." - ATRIBUT PRO LOG DUMP: SLOUPEC #".$colId." (".$colName.")");
                    $dumpColList[$instId][$tab]["dumpColId"]   = $colId;
                    $dumpColList[$instId][$tab]["dumpColName"] = $colName;
                    foreach ($adhocDump["needles"][(string)$instId][$tab]["dumpColVals"] as $dumpColVal) {
                        $dumpColList[$instId][$tab]["dumpColVals"][] = $dumpColVal;
                        logInfo("TABULKA ".$instId."_".$tab." - PŘIDÁNA DUMPOVACÍ HODNOTA ".$colName." = ".$dumpColVal);
                    }                    
                }
            }
            $colId ++;                                                          // přechod na další sloupec            
        }        
    }
}
//var_dump($adhocDump, $dumpColList);
logInfo("DOKONČENO NAČTENÍ DEFINICE DATOVÉHO MODELU");
$expectedDigs = $idFormat["instId"] + $idFormat["idTab"];
logInfo("PŘEDPOKLÁDANÁ DÉLKA INDEXŮ VE VÝSTUPNÍCH TABULKÁCH JE ".$expectedDigs." ČÍSLIC");  // volitelný diagnostický výstup do logu
// ==============================================================================================================================================================================================

logInfo("ZAHÁJENO ZPRACOVÁNÍ DAT");     // volitelný diagnostický výstup do logu
$idFormatIdEnoughDigits = false;        // příznak potvrzující, že počet číslic určený proměnnou $idFormat["idTab"] dostačoval k indexaci záznamů u všech tabulek (vč. out-only položek)
$tabItems = [];                         // pole počitadel záznamů v jednotlivých tabulkách (ke kontrole nepřetečení počtu číslic určeném proměnnou $idFormat["idTab"])

while (!$idFormatIdEnoughDigits) {      // dokud není potvrzeno, že počet číslic určený proměnnou $idFormat["idTab"] dostačoval k indexaci záznamů u všech tabulek (vč. out-only položek)
    
    foreach ($tabs_InOut_OutOnly[6] as $tab => $cols) {        
        $tabItems[$tab] = 0;                                // úvodní nastavení nulových hodnot počitadel počtu záznamů všech OUT tabulek
        // vytvoření výstupních souborů    
        ${"out_".$tab} = new \Keboola\Csv\CsvFile($dataDir."out".$ds."tables".$ds."out_".$tab.".csv");
        // zápis hlaviček do výstupních souborů
        $colsOut = array_key_exists($tab, $colsInOnly) ? array_diff(array_keys($cols), $colsInOnly[$tab]) : array_keys($cols);
        $colPrf  = strtolower($tab)."_";                    // prefix názvů sloupců ve výstupní tabulce (např. "loginSessions" → "loginsessions_")
        $colsOut = preg_filter("/^/", $colPrf, $colsOut);   // prefixace názvů sloupců ve výstupních tabulkách názvy tabulek kvůli rozlišení v GD (např. "title" → "groups_title")
        ${"out_".$tab} -> writeRow($colsOut); 
    }
    logInfo("VÝSTUPNÍ SOUBORY VYTVOŘENY, ZÁHLAVÍ VLOŽENA"); // volitelný diagnostický výstup do logu

    // vytvoření záznamů s umělým ID v tabulkách definovaných proměnnou $tabsFakeRow (kvůli JOINu tabulek v GoodData) [volitelné]
    if ($emptyToNA) {
        /*foreach ($tabsFakeRow as $ftab) {
            $frow = array_merge([$fakeId, $fakeTitle], array_fill(2, $outTabsColsCount[$ftab] - 2, ""));
            ${"out_".$ftab} -> writeRow($frow);
            logInfo("VLOŽEN UMĚLÝ ZÁZNAM S ID \"".$fakeId."\" A NÁZVEM \"".$fakeTitle."\" DO VÝSTUPNÍ TABULKY ".$ftab); // volitelný diag. výstup do logu
        }               // umělý řádek do aktuálně iterované tabulky ... ["0", "", "", ... , ""]          
        */
        foreach ($tabsFakeRow as $ftab) {
            $frow = [];
            foreach ($tabs_InOut_OutOnly[6] + $tabs_InOut_OutOnly[5] as $tabName => $col) {
                foreach ($col as $colName => $colAttrs) {
                    if ($tabName == $ftab) {
                        if (array_key_exists($tabName, $colsInOnly)) {
                            if (in_array($colName, $colsInOnly[$tabName])) {continue;}                  // sloupec je in-only → přeskočit 
                        }
                        if (array_key_exists("fk", $colAttrs) || array_key_exists("pk", $colAttrs)) {   // do sloupců typu FK nebo PK ...
                            $frow[] = $fakeId;                                                          // ... se vloží $fakeId, ...
                        } elseif (array_key_exists("tt", $colAttrs)) {
                            $frow[] = $fakeTitle;                                                       // ... do sloupců obsahujících title se vloží $fakeTitle, ...
                        } else {
                            $frow[] = "";                                                               // ... do ostatních sloupců se vloží ptázdná hodnota
                        }
                    }
                }
            }
            ${"out_".$ftab} -> writeRow($frow);
            logInfo("VLOŽEN UMĚLÝ ZÁZNAM S ID \"".$fakeId."\" A NÁZVEM \"".$fakeTitle."\" DO VÝSTUPNÍ TABULKY ".$ftab); // volitelný diag. výstup do logu
        }
        // $out_groups -> writeRow([$fakeId, $fakeTitle]);  // není třeba
    }
    
    // načtení záznamů z tabulky "actItems" importované z OUT bucketu (jako zdroj existujícího číselníku actItems) do pole $actItems
    $actItems = [];                                                                                     // prvek pole má tvar <name> => <idactitem>
    $in_actItems = new Keboola\Csv\CsvFile($dataDir."in".$ds."tables".$ds."in_actItems.csv");           // načtení souboru
    foreach ($in_actItems as $rowNum => $row) {                                                         // načítání řádků tabulky "actItems" z out-bucketu
        if ($rowNum == 0) {continue;}                                                                   // vynechání hlavičky tabulky
        $actItems[$row[1]] = $row[0];                                                                   // přidání prvku <name> => <idactitem>
    }
    logInfo("NAČTEN ČÍSELNÍK actItems Z OUT-BUCKETU");                                                  // volitelný diagnostický výstup do logu 
    // ==========================================================================================================================================================================================
    // zápis záznamů do výstupních souborů

    // [A] tabulky sestavené ze záznamů více instancí (záznamy ze všech instancí se zapíší do stejných výstupních souborů

    setFieldsShift();                                       // výpočet konstant posunu indexování formulářových polí
    initStatuses();                                         // nastavení výchozích hodnot proměnných popisujících stavy
    initGroups();                                           // nastavení výchozích hodnot proměnných popisujících skupiny
    initDbGroups();                                         // nastavení výchozích hodnot proměnných popisujících skupiny databází
    
    foreach ($instCommonOuts as $tab => $common) {
        switch ($common) {
            case 0: ${"common".ucfirst($tab)}=false; break; // záznamy v tabulce budou indexovány pro každou instanci zvlášť
            case 1: ${"common".ucfirst($tab)}=true;         // záznamy v tabulce budou indexovány pro všechny instance společně
        }
    }
    
    // iterace instancí -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    foreach ($instances as $instId => $inst) {              // procházení tabulek jednotlivých instancí Daktela
        initFields();                                       // nastavení výchozích hodnot proměnných popisujících formulářová pole         
        if (!$commonStatuses)       {initStatuses();    }   // ID a názvy v tabulce 'statuses' požadujeme uvádět pro každou instanci zvlášť    
        if (!$commonGroups)         {initGroups();      }   // ID a názvy v out-only tabulce 'groups' požadujeme uvádět pro každou instanci zvlášť
        if (!$commonDatabaseGroups) {initDbGroups();    }   // ID a názvy v out-only tabulce 'databaseGroups' požadujeme uvádět pro každou instanci zvlášť
        logInfo("ZAHÁJENO ZPRACOVÁNÍ INSTANCE ".$instId);   // volitelný diagnostický výstup do logu
        
        // iterace tabulek dané instance --------------------------------------------------------------------------------------------------------------------------------------------------------
        foreach ($tabs_InOut_InOnly[$inst["ver"]] as $tab => $cols) {               // iterace tabulek dané instance
            $dateRestrictColId = dateRestrictColId($instId, $tab);                  // ID sloupce, který je v dané tabulce atributem pro datumovou restrikci (0,1,...), pokud v tabulce existuje (jinak NULL)
            if (!$inst["instOn"] && !is_null($dateRestrictColId)) {                 // jde o dynamickou tabulku v instanci vypnuté v konfiguračním JSONu
                logInfo("ZPRACOVÁNÍ TABULKY ".$instId."_".$tab." VYPNUTO V JSON");  // volitelný diagnostický výstup do logu 
                continue;
            }
            logInfo("ZAHÁJENO ZPRACOVÁNÍ TABULKY ".$instId."_".$tab);               // volitelný diagnostický výstup do logu           
            // iterace řádků dané tabulky -------------------------------------------------------------------------------------------------------------------------------------------------------
            foreach (${"in_".$tab."_".$instId} as $rowNum => $row) {                // načítání řádků vstupních tabulek [= iterace řádků]
                if ($rowNum == 0) {continue;}                                       // vynechání hlavičky tabulky
                // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                // při inkrementáním módu zpracování pro všechny nestatické tabulky (tj. nejen "activities" a z nich odvozené) přeskočení záznamů ležících mimo zpracovávaný datumový rozsah 
                if (!$incrActivitiesOnly && !is_null($dateRestrictColId)) {         // inkrementálně zpracováváme všechny nestatické tabulky && sloupec pro datumovou restrikci záznamů v tabulce existuje
                    if (!dateRngCheck($row[$dateRestrictColId])) {continue;}        // hodnota atributu pro datumovou restrikci leží mimo zpracovávaný datumový rozsah → přechod na další řádek                  
                } 
                // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                $tabItems[$tab]++;                                                  // inkrement počitadla záznamů v tabulce
                if (checkIdLengthOverflow($tabItems[$tab])) {                       // došlo k přetečení délky ID určené proměnnou $idFormat["idTab"]
                    continue 4;                                                     // zpět na začátek cyklu 'while' (začít plnit OUT tabulky znovu, s delšími ID)
                }
          
                $colVals = $fieldRow = [];                                          // řádek obecné výstupní tabulky | záznam do pole formulářových polí     
                $callsVals= $emailsVals= $smsVals= $chatsVals= $commentsVals= [];   // řádek výstupní tabulky 'calls' | 'mails' | 'sms' | 'chats' | 'comments' 
                unset ($idFieldSrcRec, $idactivity, $idticket, $idqueue, $iduser, $idrecord, $idstatus, $type, $description, $time, $timeOpen, $timeClose, $npsScore, $npsNote, $dumpColName);
                //                                                                  // reset indexu zdrojového záznamu do out-only tabulky hodnot formulářových polí + ...
                                                                                    // ... + indexu zdrojové aktivity do out-only tabulky 'actItemVals' + ID front, uživatelů, typu aktivity + ID statusu                               
                $colId = 0;                                                         // index sloupce (v každém řádku číslovány sloupce 0,1,2,...) 
       
                foreach ($cols as $colName => $colAttrs) {                          // konstrukce řádku výstupní tabulky (vložení hodnot řádku) [= iterace sloupců]                    
                    if($colName == arrVal([$instId, $tab, "dumpColName"], $dumpColList)) { $dumpColName = $colName; }
                    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                    switch ($colAttrs["instPrf"]) {                                 // prefixace hodnoty číslem instance (je-li požadována)
                        case 0: $hodnota = $row[$colId]; break;                     // hodnota bez prefixu instance
                        case 1: $hodnota = setIdLength($instId, $row[$colId]);      // hodnota s prefixem instance
                    }
                    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                    $afterJsonProc = jsonProcessing($instId,$tab,$colName,$hodnota);// jsonProcessing - test, zda je ve sloupci JSON; když ano, rozparsuje se
                    if (!$afterJsonProc) {$colId++; continue;}                      // přechod na další sloupec
                    
                    $colParentTab = colParentTab($instId, $tab, $colName);          // test, zda je daný sloupec FK; když ano, aplikuje se na hodnotu fce emptyToNA (u FK vrátí název nadřazené tabulky, u ne-FK NULL)
                    $hodnota = is_null($colParentTab) ? $hodnota : emptyToNA($hodnota); // emptyToNA - prázdné hodnoty nahradí $fakeId kvůli integritní správnosti
                    
                    switch ([$tab, $colName]) {
                        // TABULKY V5+6
                        case ["users", "login"]:    $colVals[] = convertLogin($hodnota);                        // je-li loginem (v Daktele users.name) x-kód (platí u Daktely v6), odstraní se počáteční "x"
                                                    break;
                        case ["users", "location"]: $colVals[] = subparamParse(descriptParse($hodnota),"LOC:"); // název lokality parsovaný z users.location (v Daktele users.description) pomocí delimiterů
                                                    break;
                        case ["pauses", "paid"]:    $colVals[] = boolValsUnify($hodnota);                       // dvojici bool. hodnot ("",1) u v6 převede na dvojici hodnot (0,1) používanou u v5                                 
                                                    break;
                        case ["queues", "idgroup"]: $groupName = descriptParse($hodnota);                       // název skupiny parsovaný z queues.idgroup pomocí delimiterů
                                                    if (empty($groupName)) {                                    // název skupiny ve vstupní tabulce 'queues' nevyplněn ...
                                                        $colVals[] = emptyToNA($groupName);  break;             // ... → ID skupiny se do výstupní tabulky 'queues' nezapíše
                                                    }  
                                                    if (!array_key_exists($groupName, $groups)) {               // skupina daného názvu dosud není uvedena v poli $groups 
                                                        $idGroup++;                                             // inkrement umělého ID skupiny   
                                                        if (checkIdLengthOverflow($idGroup)) {                  // došlo k přetečení délky ID určené proměnnou $idGroup
                                                                continue 6;                                     // zpět na začátek cyklu 'while' (začít plnit OUT tabulky znovu, s delšími ID)
                                                            }
                                                        $idGroupFormated = setIdLength($instId,$idGroup,!$commonGroups);// $commonGroups → neprefixovat $idGroup identifikátorem instance
                                                        $groups[$groupName] = $idGroupFormated;                 // zápis skupiny do pole $groups
                                                        $out_groups -> writeRow([$idGroupFormated,$groupName]); // zápis řádku do out-only tabulky 'groups' (řádek má tvar idgroup | groupName)                                                                                                                                                              
                                                    } else {
                                                        $idGroupFormated = $groups[$groupName];                 // získání idgroup dle názvu skupiny z pole $groups
                                                    }                                                
                                                    $colVals[] = $idGroupFormated;                              // vložení formátovaného ID skupiny do konstruovaného řádku výstupní tabulky 'queues'
                                                    break;
                        case ["calls", "call_time"]:if (!dateRngCheck($hodnota)) {                              // 'call_time' není z požadovaného rozsahu -> ...
                                                        continue 3;                                             // ... řádek z tabulky 'calls' přeskočíme
                                                    } else {                                                    // 'call_time' je z požadovaného rozsahu -> ...
                                                        $colVals[] = $hodnota; break;                           // ... 'call_time' použijeme a normálně pokračujeme v konstrukci řádku...
                                                    }                        
                        case ["calls", "answered"]: $colVals[] = boolValsUnify($hodnota);                       // dvojici bool. hodnot ("",1) u v6 převede na dvojici hodnot (0,1) používanou u v5                                 
                                                    break;
                        case ["calls", "iduser_activity"]:
                                                    $colVals[] = emptyToNA($hodnota);                           // u v5 jsou v calls.iduser_activity všude prázdné hodnoty                           
                                                    break;
                        case ["calls", "idrecord"]: $colVals[] = emptyToNA($hodnota);                           // u v5 jsou v calls.idrecord všude prázdné hodnoty
                                                    break;
                        case ["calls", "clid"]:     $colVals[] = phoneNumberCanonic($hodnota);                  // veřejné tel. číslo v kanonickém tvaru (bez '+')
                                                    break;
                        case ["calls", "contact"]:  $colVals[] = "";                                            // zatím málo využívané pole, obecně objekt (JSON), pro použití by byl nutný json_decode
                                                    break;
                        case["statuses","idstatus"]:if ($commonStatuses) {                                      // ID a názvy v tabulce 'statuses' požadujeme společné pro všechny instance  
                                                        $statIdOrig = $hodnota;                                 // uložení originálního (prefixovaného) ID stavu do proměnné $statIdOrig
                                                    } else {                                                    // ID a názvy v tabulce 'statuses' požadujeme uvádět pro každou instanci zvlášť
                                                        $colVals[]  = $hodnota;                                 // vložení formátovaného ID stavu jako prvního prvku do konstruovaného řádku
                                                    }              
                                                    break;
                        case ["statuses", "title"]: if ($commonStatuses) {                                      // ID a názvy v tabulce 'statuses' požadujeme společné pro všechny instance
                                                        $iterRes = iterStatuses($hodnota, "title");             // výsledek hledání title v poli $statuses (umělé ID stavu nebo false)
                                                        if (!$iterRes) {                                        // stav s daným title dosud v poli $statuses neexistuje
                                                            $idStatus++;                                        // inkrement umělého ID stavů
                                                            if (checkIdLengthOverflow($idStatus)) {             // došlo k přetečení délky ID určené proměnnou $idStatus
                                                                continue 6;                                     // zpět na začátek cyklu 'while' (začít plnit OUT tabulky znovu, s delšími ID)
                                                            }
                                                            $statuses[$idStatus]["title"]          = $hodnota;  // zápis hodnot stavu do pole $statuses
                                                            $statuses[$idStatus]["statusIdOrig"][] = $statIdOrig;
                                                            $colVals[] = setIdLength(0, $idStatus, false);      // vložení formátovaného ID stavu jako prvního prvku do konstruovaného řádku                                        

                                                        } else {                                                // stav s daným title už v poli $statuses existuje
                                                            $statuses[$iterRes]["statusIdOrig"][] = $statIdOrig;// připsání orig. ID stavu jako dalšího prvku do vnořeného 1D-pole ve 3D-poli $statuses
                                                            break;                                              // aktuálně zkoumaný stav v poli $statuses už existuje
                                                        }
                                                        unset($statIdOrig);                                     // unset proměnné s uloženou hodnotou originálního (prefixovaného) ID stavu (úklid)
                                                    }                                             
                                                    $colVals[] = $hodnota;                                      // vložení title stavu jako druhého prvku do konstruovaného řádku                                           
                                                    break;
                        case ["recordSnapshots", "idstatus"]:
                                                    $colVals[] = $commonStatuses ? setIdLength(0, iterStatuses($hodnota), false) : $hodnota;
                                                    break;
                        case ["fields", "idfield"]: // varianta pro striktně numerické číslování instancí (0÷9):
                                                    //$hodnota_shift = (int)$hodnota + $formFieldsIdShift;
                                                    // varianta pro alfanumerické číslování instancí (0÷9, A, B, ...):
                                                    $hodnota_shift = substr($hodnota, 0, $idFormat["instId"]) . $formFieldsIdShift . substr($hodnota, $idFormat["instId"]+1, strlen($hodnota)-$idFormat["instId"]-1);
                                                    $colVals[] = $fieldRow["idfield"] = $hodnota_shift;         // hodnota záznamu do pole formulářových polí
                                                    break;
                        case ["fields", "title"]:   $colVals[] = $fieldRow["title"]= $hodnota;                  // hodnota záznamu do pole formulářových polí
                                                    break;
                        case ["fields", "name"]:    $fieldRow["name"] = $hodnota;               // název klíče záznamu do pole formulářových polí
                                                    break;                                      // sloupec "name" se nepropisuje do výstupní tabulky "fields"                
                        case ["records","idrecord"]:$idFieldSrcRec = $colVals[] = $hodnota;     // uložení hodnoty 'idrecord' pro následné použití ve 'fieldValues'
                                                    break;
                        case ["records","idstatus"]:$colVals[] = $commonStatuses ? setIdLength(0, iterStatuses($hodnota), false) : $hodnota;
                                                    break;
                        case ["records", "number"]: $colVals[] = phoneNumberCanonic($hodnota);  // veřejné tel. číslo v kanonickém tvaru (bez '+')
                                                    break;
                        case ["records", "action"]: $colVals[] = actionCodeToName($hodnota);    // číselný kód akce převedený na název akce
                                                    break;                                               
                        case [$tab,"idinstance"]:   $colVals[] = $instId;  break;               // hodnota = $instId    
                        // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------                                          
                        // TABULKY V6 ONLY
                        case ["databases", "iddatabasegroup"]:                                                  // v IN bucketu má sloupec název "description"
                                                    $dbGroupName = descriptParse($hodnota, true);               // název skupiny databází parsovaný z databases.description pomocí delimiterů
                                                    if (!strlen($dbGroupName)) {                                // název skupiny databází ve vstupní tabulce 'databases' nevyplněn ...
                                                        $colVals[] = "";  break;                                // ... → ID skupiny databází se do výstupní tabulky 'databases' nezapíše
                                                    }                                                           // (místo '!strlen' nelze použít 'empty' kvůli vyhodnocení $fakeId = '0')
                                                    if (!array_key_exists($dbGroupName, $dbGroups)) {           // skupina databází daného názvu dosud není uvedena v poli $dbGroups 
                                                        $idDbGroup++;                                           // inkrement umělého ID skupiny databází   
                                                        if (checkIdLengthOverflow($idDbGroup)) {                // došlo k přetečení délky ID určené proměnnou $idDbGroup
                                                                continue 6;                                     // zpět na začátek cyklu 'while' (začít plnit OUT tabulky znovu, s delšími ID)
                                                            }
                                                        $idDbGroupFormated = setIdLength($instId,$idDbGroup,!$commonDatabaseGroups);// $commonDatabaseGroups → neprefixovat $idDbGroup identifikátorem instance
                                                        $dbGroups[$dbGroupName] = $idDbGroupFormated;           // zápis skupiny databází do pole $dbGroups
                                                        $out_databaseGroups -> writeRow([$idDbGroupFormated,$dbGroupName]); // zápis řádku do out-only tabulky 'databaseGroups' (řádek má tvar iddatabasegroup | dbGroupName)
                                                    } else {
                                                        $idDbGroupFormated = $dbGroups[$dbGroupName];           // získání iddatabasegroup dle názvu skupiny databází z pole $dbGroups
                                                    }                                                
                                                    $colVals[] = $idDbGroupFormated;                            // vložení formátovaného ID skupiny do konstruovaného řádku výstupní tabulky 'databases'
                                                    break;
                        case ["contacts","idcontact"]:$idFieldSrcRec = $colVals[]= $hodnota;    // uložení hodnoty 'idcontact' pro následné použití v 'contFieldVals'
                                                    break;
                        case ["contacts", "form"]:  // parsování "number" (veřejného tel. číslo) pro potřeby CRM records reportu
                                                    $telNum = "";
                                                    $numArr = getJsonItem($hodnota, "number");  // obecně vrací 1D-pole tel. čísel → beru jen první číslo
                                                    if (is_array($numArr)) {
                                                        if (array_key_exists(0, $numArr)) {
                                                            $telNum = phoneNumberCanonic($numArr[0]); // uložení tel. čísla do proměnné $telNum
                                                        }
                                                    }
                                                    break;                          // sloupec "form" se nepropisuje do výstupní tabulky "contacts"  
                        case ["contacts","number"]: $colVals[] = $telNum;           // hodnota vytvořená v case ["contacts", "form"]
                                                    break;
                        case ["tickets","idticket"]:$idFieldSrcRec = $colVals[] = $hodnota; // uložení hodnoty 'idticket' pro následné použití v 'tickFieldVals'
                                                    break;
                        case ["tickets", "email"]:  $colVals[] = convertMail($hodnota);
                                                    break;
                        case ["tickets","idstatus"]:$colVals[] = $commonStatuses ? setIdLength(0, iterStatuses($hodnota), false) : $hodnota;
                                                    break;
                        case ["tickets","description"]: // normalizovaná hodnota - vyřazeny texty obsahující HTML, bez multiplicitního výskytu podřetězců, přebytečných mezer, ořezaná
                                                    $colVals[] = is_string($hodnota) ? strLenRestrict_htmlThrow_remStrMultipl_trimAll($hodnota) : "";
                                                    break;
                        case ["crmRecords", "idcrmrecord"]:
                                                    $idFieldSrcRec = $colVals[]= $hodnota;   // uložení hodnoty 'idcrmrecord' pro následné použití v 'crmFieldVals'
                                                    break;
                        case ["crmRecords", "idstatus"]:
                                                    $colVals[] = $commonStatuses ? setIdLength(0, iterStatuses($hodnota), false) : $hodnota;
                                                    break;
                        case ["crmRecordSnapshots", "idstatus"]:
                                                    $colVals[] = $commonStatuses ? setIdLength(0, iterStatuses($hodnota), false) : $hodnota;
                                                    break;
                        case ["activities", "idactivity"]:
                                                    $colVals[] = $idactivity = $hodnota;    // $idactivity ... pro použití u parsování JSONu v "activities"."item" (→ 'actItemVals')
                                                    break;
                        case ["activities", "name"]:$colVals[] = $activityName = $hodnota;  // $activityName ... pro vložení do tabulky 'calls'
                                                    break;
                        case ["activities", "title"]:
                                                    $colVals[] = is_numeric($hodnota) ? phoneNumberCanonic($hodnota) : $hodnota;
                                                    break;                                  // 'title' standardně obsahuje tel. číslo v různých formátech → je třeba kanonizovat
                        case ["activities", "idticket"]:
                                                    $colVals[] = $idticket = $hodnota;      // $idticket ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "idqueue"]:
                                                    $colVals[] = $idqueue = $hodnota;       // $idqueue ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "iduser"]:
                                                    $colVals[] = $iduser = $hodnota;        // $iduser  ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "idrecord"]:
                                                    $colVals[] = $idrecord = $hodnota;      // $idrecord  ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "idstatus"]:
                                                    $colVals[] = $idstatus = $commonStatuses ? setIdLength(0, iterStatuses($hodnota), false) : $hodnota;    // $idstatus  ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "type"]:$colVals[] = $type = $hodnota;          // $type ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "description"]:
                                                    $colVals[] = $description = $hodnota;   // $description ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "time"]:$colVals[] = $time = $hodnota;          // $time ... pro použití v case ["activities", "item"]
                                                    break;       
                        case ["activities", "time_open"]:
                                                    $colVals[] = $timeOpen = $hodnota;      // $timeOpen ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "time_close"]:
                                                    $colVals[] = $timeClose = $hodnota;     // $timeClose ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "nps_score"]:
                                                    $colVals[] = $npsScore = $hodnota;      // $npsScore ... pro použití v case ["activities", "item"]
                                                    break;
                        case ["activities", "nps_note"]:
                                                    $colVals[] = $npsNote = strLenRestrict(strip_tags($hodnota));
                                                    break;                                  // $npsNote ... pro použití v case ["activities", "item"]
                        case ["activities", "item"]:$colVals[] = $hodnota;                  // obecně objekt (JSON), propisováno do OUT bucketu i bez parsování (potřebuji 'duration' v performance reportu)
                                                    if ($type != '') {                      // jiný typ aktivity než comment
                                                        $item = json_decode($hodnota, true, JSON_UNESCAPED_UNICODE);                        
                                                        if (!is_array($item)) {break;}      // hodnota dekódovaného JSONu není pole (např. je null) → nelze ji prohledávat jako pole (pův. podmínka jen is_null)
                                                    }
                                                    switch (strtolower($type)) {
                                                        case "call":                        // pro aktivity typu CALL pokračovat sestavením hodnot do tabulky 'calls'
                                                                    if (!is_null(arrVal("call_time", $item))) {
                                                                        if (!dateRngCheck($item["call_time"])) {continue 4;}    // 'call_time' není z požadovaného rozsahu → řádek z tabulky 'activities' přeskočíme                                                 
                                                                    }
                                                                    if (!is_null(arrVal("id_call", $item))) {
                                                                        if (empty($item["id_call"])) {break 2;}                 
                                                                    } else {
                                                                        break 2;                                                // nevyplněno idcall ... → chybí PK záznamu do tabulky 'calls' ...
                                                                    }                                                           // ... → ukončení iterace prvku ve sloupci 'activities.item'
                                                                    
                                                                    $chanPars = channelsParse($item);                           // analýza hovorových kanálů (výstupem je pole $chanPars)
                                                                    $duration = $chanPars["duration_channels"] > 0 ? $chanPars["duration_channels"] : arrVal("duration", $item); 
                                                                                                                                // délka hovoru - přednostně brát z channels; je-li 0 (např. channels = null), brát item.duration
                                                                    $callTime    = arrVal("call_time"   , $item);
                                                                    $ringingTime = arrVal("ringing_time", $item);
                                                                    
                                                                    // idqueue, idrecord, idstatus - hodnoty druhé volby (z JSONu $item) - pro použití v případech, ...
                                                                    // ... kdy nejsou tyto hodnoty vyplněny ve sloupcích calls.idqueue, calls.idrecord, calls.idstatus ...
                                                                    // ... (typicky u 2. části přepojeného hovoru):
                                                                    $item_idqueue  = emptyToNA(setIdLength($instId, arrVal(["id_queue","_sys","id"], $item)));
                                                                    $item_idrecord = $chanPars["idrecord"]; // ver. <= 6.17:  emptyToNA(setIdLength($instId, arrVal(["channels",1,"activity","record","_sys","id"]     , $item)));
                                                                    $item_idstatus = $chanPars["idstatus"]; // ver. <= 6.17:  emptyToNA(setIdLength($instId, arrVal(["channels",1,"activity","statuses",0,"_sys","id"] , $item)));
                                                                    
                                                                    $callsVals = [                                              // konstrukce řádku výstupní tabulky 'calls'
                                                                        setIdLength($instId, arrVal("id_call", $item)),
                                                                        arrVal("call_time", $item),
                                                                        arrVal("direction", $item),
                                                                        boolValsUnify(arrVal("answered", $item)),
                                                                        $idqueue == $fakeId || empty($idqueue) ? $item_idqueue : $idqueue,          // přednostně brát activities.idqueue ($idqueue); je-li nevyplněno, brát idqueue z JSONu $item
                                                                        emptyToNA(setIdLength($instId, arrVal(["id_agent","_sys","id"], $item))),   // calls_iduser          ... bráno z JSONu activities_item
                                                                        $iduser,                                                                    // calls_iduser_activity ... bráno ze sloupce activities_iduser
                                                                        $idrecord == $fakeId || empty($idrecord) ? $item_idrecord : $idrecord,      // přednostně brát activities.idrecord ($idrecord); je-li nevyplněno, brát idrecord z JSONu $item
                                                                        phoneNumberCanonic(arrVal("clid", $item)),
                                                                        emptyToNA(setIdLength($instId, arrVal(["contact","_sys","id"], $item))),
                                                                        arrVal("did", $item),
                                                                        arrVal("waiting_time", $item),
                                                                        arrVal("ringing_time", $item),
                                                                        arrVal("hold_time", $item),
                                                                        arrVal("duration", $item),
                                                                        arrVal("orig_pos", $item),
                                                                        arrVal("position", $item),
                                                                        arrVal("disposition_cause", $item),
                                                                        arrVal("disconnection_cause", $item),
                                                                        arrVal("pressed_key", $item),
                                                                        arrVal("missed_call", $item),
                                                                        arrVal("missed_call_time", $item),
                                                                        arrVal("score", $item),
                                                                        arrVal("note", $item),
                                                                        arrVal("attemps", $item),
                                                                        setIdLength($instId, arrVal("qa_user_id", $item)),
                                                                        $instId,
                                                                        $idstatus == $fakeId || empty($idstatus) ? $item_idstatus : $idstatus,  // přednostně brát activities.idstatus ($idstatus); je-li nevyplněno, brát idstatus z JSONu $item
                                                                        $duration,                                                              // calls_duration_channels (délka hovoru) - přednostně bráno z channels; je-li 0 (např. channels = null), bráno item.duration
                                                                        $chanPars["transfered"] ? arrVal("duration", $item, 0) - $chanPars["duration_channels"] : 0,  // calls_transfer_time
                                                                        $chanPars["transfer_number"],                                           // calls_transfer_number (pole $channels generované fcí channelsParse už vrací kanonické)
                                                                        getACW($timeOpen, $timeClose, $duration),                               // calls_acw     (= délka aktivity složené z hovoru a ACW minus délka hovoru)
                                                                        getACWout($timeClose, $callTime, $ringingTime, $duration),              // calls_acw_out (= $timeClose - ($callTime + $ringingTime + $duration))
                                                                        getBCW($callTime, $timeOpen),                                           // calls_bcw     (= $callTime - $timeOpen)
                                                                        arrVal("dialer_time", $item),                                           // dialer_time
                                                                        $idactivity,                                            // hodnota už je prefixovaná číslem instance (PK zdrojové tabulky)
                                                                        $activityName
                                                                    ];
                                                                    adhocDump($instId, $tab, $colVals, $callsVals);             // dump řádku do logu (je-li požadován pro danou aktivitu v konfiguračním JSONu v KBC)
                                                                    if (!empty($callsVals)) {                                   // je sestaveno pole pro zápis do řádku výstupní tabulky 'calls'
                                                                        $out_calls -> writeRow($callsVals);                     // zápis sestaveného řádku do výstupní tabulky 'calls'
                                                                    }
                                                                    break;
                                                        case "email":                        // pro aktivity typu EMAIL pokračovat sestavením hodnot do tabulky 'emails'
                                                                    if (!is_null(arrVal("time", $item))) {
                                                                        if (!dateRngCheck($item["time"])) {continue 4;}         // 'time' není z požadovaného rozsahu → řádek z tabulky 'activities' přeskočíme                                                 
                                                                    }
                                                                    if (!is_null(arrVal(["_sys","id"], $item))) {
                                                                        if (empty($item["_sys"]["id"])) {break 2;}              
                                                                    } else {
                                                                        break 2;                                                // nevyplněno ID e-mailu → chybí PK záznamu do tabulky 'emails' ...
                                                                    }                                                           // ... → ukončení iterace prvku ve sloupci 'activities.item'
                                                                    $emailsVals = [                                             // konstrukce řádku výstupní tabulky 'emails'
                                                                        setIdLength($instId, arrVal(["_sys","id"], $item)),
                                                                        arrVal("time", $item),
                                                                        emptyToNA($idqueue),
                                                                        emptyToNA(setIdLength($instId, arrVal(["user","_sys","id"], $item))),
                                                                        emptyToNA($iduser),
                                                                        emptyToNA($idrecord),
                                                                        convertMail(arrVal("address", $item)),
                                                                        $instId,
                                                                        $idstatus,
                                                                        arrVal("wait_time", $item),
                                                                        arrVal("duration", $item),
                                                                        boolValsUnify(arrVal("answered", $item)),
                                                                        arrVal("direction", $item),
                                                                        strLenRestrict(arrVal("title", $item)),
                                                                        $idactivity,                                            // hodnota už je prefixovaná číslem instance (PK zdrojové tabulky)
                                                                        emptyToNA($idticket),
                                                                        //htmlNoAccentsAndTags(arrVal("text", $item))           // tělo e-mailu - problematické (HTML tagy, znaky UTF-8, ...) -> zatím neimplementováno
                                                                    ];
                                                                    adhocDump($instId, $tab, $colVals, $emailsVals);            // dump řádku do logu (je-li požadován pro danou aktivitu v konfiguračním JSONu v KBC)
                                                                    if (!empty($emailsVals)) {                                  // je sestaveno pole pro zápis do řádku výstupní tabulky 'emails'
                                                                        $out_emails -> writeRow($emailsVals);                   // zápis sestaveného řádku do výstupní tabulky 'emails'
                                                                    }
                                                                    break;
                                                        case "sms": 
                                                                    if (!is_null(arrVal("time", $item))) {
                                                                        if (!dateRngCheck($item["time"])) {continue 4;}         // 'time' není z požadovaného rozsahu → řádek z tabulky 'activities' přeskočíme                                                 
                                                                    }
                                                                    if (!is_null(arrVal(["_sys","id"], $item))) {
                                                                        if (empty($item["_sys"]["id"])) {break 2;}              
                                                                    } else {
                                                                        break 2;                                                // nevyplněno ID SMS → chybí PK záznamu do tabulky 'sms' ...
                                                                    }                                                           // ... → ukončení iterace prvku ve sloupci 'activities.item'
                                                                    $smsVals = [                                                // konstrukce řádku výstupní tabulky 'sms'
                                                                        setIdLength($instId, arrVal(["_sys","id"], $item)),
                                                                        arrVal("time", $item),
                                                                        emptyToNA($idqueue),
                                                                        emptyToNA(setIdLength($instId, arrVal(["user","_sys","id"], $item))),
                                                                        emptyToNA($iduser),
                                                                        emptyToNA($idrecord),
                                                                        phoneNumberCanonic(arrVal("clid", $item)),
                                                                        $instId,
                                                                        $idstatus,
                                                                        arrVal("wait_time", $item),
                                                                        arrVal("duration", $item),
                                                                        boolValsUnify(arrVal("answered", $item)),
                                                                        arrVal("direction", $item),
                                                                        arrVal("disconnection", $item),
                                                                        $idactivity,                                            // hodnota už je prefixovaná číslem instance (PK zdrojové tabulky)
                                                                        emptyToNA($idticket)
                                                                    ];
                                                                    adhocDump($instId, $tab, $colVals, $smsVals);               // dump řádku do logu (je-li požadován pro danou aktivitu v konfiguračním JSONu v KBC)
                                                                    if (!empty($smsVals)) {                                     // je sestaveno pole pro zápis do řádku výstupní tabulky 'sms'
                                                                        $out_sms -> writeRow($smsVals);                         // zápis sestaveného řádku do výstupní tabulky 'sms'
                                                                    }
                                                                    break;
                                                        case "chat":
                                                                    if (!is_null(arrVal("time", $item))) {
                                                                        if (!dateRngCheck($item["time"])) {continue 4;}         // 'time' není z požadovaného rozsahu → řádek z tabulky 'activities' přeskočíme                                                 
                                                                    }
                                                                    if (!is_null(arrVal(["_sys","id"], $item))) {
                                                                        if (empty($item["_sys"]["id"])) {break 2;}              
                                                                    } else {
                                                                        break 2;                                                // nevyplněno ID chatu → chybí PK záznamu do tabulky 'chats' ...
                                                                    }                                                           // ... → ukončení iterace prvku ve sloupci 'activities.item'
                                                                    $chatsVals = [                                              // konstrukce řádku výstupní tabulky 'chats'
                                                                        setIdLength($instId, arrVal(["_sys","id"], $item)),
                                                                        arrVal("time", $item),
                                                                        emptyToNA($idqueue),
                                                                        emptyToNA(setIdLength($instId, arrVal(["user","_sys","id"], $item))),
                                                                        emptyToNA($iduser),
                                                                        emptyToNA($idrecord),
                                                                        convertMail(arrVal("email", $item)),
                                                                        $instId,
                                                                        $idstatus,
                                                                        arrVal("wait_time", $item),
                                                                        arrVal("duration", $item),
                                                                        boolValsUnify(arrVal("answered", $item)),
                                                                        strLenRestrict(arrVal("title", $item)),
                                                                        arrVal("disconnection", $item),
                                                                        $npsScore,
                                                                        $npsNote,
                                                                        $idactivity,                                            // hodnota už je prefixovaná číslem instance (PK zdrojové tabulky)
                                                                        emptyToNA($idticket)
                                                                    ];
                                                                    adhocDump($instId, $tab, $colVals, $chatsVals);             // dump řádku do logu (je-li požadován pro danou aktivitu v konfiguračním JSONu v KBC)
                                                                    if (!empty($chatsVals)) {                                   // je sestaveno pole pro zápis do řádku výstupní tabulky 'chats'
                                                                        $out_chats -> writeRow($chatsVals);                     // zápis sestaveného řádku do výstupní tabulky 'chats'
                                                                    }
                                                                    break;
                                                        case "":                                                                // activities.type = "" ... comment
                                                                    if (!empty($time)) {
                                                                        if (!dateRngCheck($time)) {continue 4;}                 // 'time' není z požadovaného rozsahu → řádek z tabulky 'activities' přeskočíme                                                 
                                                                    }
                                                                    if (empty($idactivity)) {                                   // nevyplněno ID aktivity → chybí PK záznamu do tabulky 'comments' ...
                                                                        break 2;                                                // ... → ukončení iterace prvku ve sloupci 'activities.item'
                                                                    }                                                           
                                                                    $commentsVals = [                                           // konstrukce řádku výstupní tabulky 'chats'
                                                                        $idactivity,                                            // ID komentáře = ID aktivity (hodnota už je prefixovaná číslem instance)
                                                                        $time,
                                                                        emptyToNA($idqueue),
                                                                        emptyToNA($iduser),
                                                                        emptyToNA($idrecord),
                                                                        emptyToNA($idticket),
                                                                        $instId,
                                                                        $idstatus,
                                                                        remNonprocessableStrings($description)                  // do 29. 9. 2023: strLenRestrict(strip_tags($description)) - výstup obsahuje neexportovatelné znaky
                                                                    ];
                                                                    adhocDump($instId, $tab, $colVals, $commentsVals);          // dump řádku do logu (je-li požadován pro danou aktivitu v konfiguračním JSONu v KBC)
                                                                    if (!empty($commentsVals)) {                                // je sestaveno pole pro zápis do řádku výstupní tabulky 'chats'
                                                                        $out_comments -> writeRow($commentsVals);               // zápis sestaveného řádku do výstupní tabulky 'chats'
                                                                    }
                                                                    break;
                                                        default:    break 2;                            // ukončení iterace prvku ve sloupci 'activities.item'
                                                    }    
                                                    break;
                        case ["crmFields", "idcrmfield"]:
                                                    // varianta pro striktně numerické číslování instancí (0÷9):
                                                    //$hodnota_shift = (int)$hodnota + $formCrmFieldsIdShift;
                                                    // varianta pro alfanumerické číslování instancí (0÷9, A, B, ...):
                                                    $hodnota_shift = substr($hodnota, 0, $idFormat["instId"]) . $formCrmFieldsIdShift . substr($hodnota, $idFormat["instId"]+1, strlen($hodnota)-$idFormat["instId"]-1);
                                                    $colVals[] = $fieldRow["idfield"] = $hodnota_shift; // hodnota záznamu do pole formulářových polí
                                                    break;
                        case ["crmFields", "title"]:$colVals[] = $fieldRow["title"] = $hodnota;         // hodnota záznamu do pole formulářových polí
                                                    break;
                        case ["crmFields", "name"]: $fieldRow["name"] = $hodnota;                       // název klíče záznamu do pole formulářových polí
                                                    break;                                              // sloupec "name" se nepropisuje do výstupní tabulky "fields"  
                        case ["qaReviews", "note"]: $colVals[] = strLenRestrict_htmlThrow_remStrMultipl_trimAll($hodnota);  // limitace délky popisu poslecháře + vynechání odřádkování a prázdných znaků atd.
                                                    break;                                                          
                        // ----------------------------------------------------------------------------------------------------------------------------------------------------------------------                                                  
                        default:                    $colVals[] = $hodnota;          // propsání hodnoty ze vstupní do výstupní tabulky bez úprav (standardní mód)
                    }                    
                    $colId++;                                                       // přechod na další sloupec (buňku) v rámci řádku                
                }   // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------              
                // operace po zpracování dat v celém řádku

                // přidání řádku do pole formulářových polí $fields (struktura pole je <idfield> => ["name" => <hodnota>, "title" => <hodnota>] )
                if (!empty($fieldRow["name"]) && !empty($fieldRow["idfield"]) && !empty($fieldRow["title"])) {  // je-li známý název, title i hodnota záznamu do pole form. polí...          
                        /*if ($instId == "3" && ($tab == "crmFields" || $tab == "fields")) {
                        echo "do pole 'fields' přidán záznam (idfield ".$fieldRow["idfield"].", name ".$fieldRow["name"].", title ".$fieldRow["title"].")\n";
                        } */
                    $fields[$fieldRow["idfield"]]["name"]  = $fieldRow["name"];     // ... provede se přidání prvku <idfield>["name"] => <hodnota> ...
                    $fields[$fieldRow["idfield"]]["title"] = $fieldRow["title"];    // ... a prvku <idfield>["title"] => <hodnota>
                } 
                
                adhocDump($instId, $tab, $colVals);                                 // dump hodnoty řádku do logu (je-li požadován v konfiguračním JSONu v KBC)
                
                $tabOut = ($tab != "crmFields") ? $tab : "fields";                  // záznamy z in-only tabulky 'crmFields' zapisujeme do in-out tabulky 'fields' 

                if (!empty($colVals)) {                                             // je sestaveno pole pro zápis do řádku výstupní tabulky
                    ${"out_".$tabOut} -> writeRow($colVals);                        // zápis sestaveného řádku do výstupní tabulky
                }
            }   // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            // operace po zpracování dat v celé tabulce
            logInfo("DOKONČENO ZPRACOVÁNÍ TABULKY ".$instId."_".$tab);              // volitelný diagnostický výstup do logu
        }
        logInfo("DOKONČENO ZPRACOVÁNÍ INSTANCE ".$instId);                      // volitelný diagnostický výstup do logu
        // operace po zpracování dat ve všech tabulkách jedné instance
                //echo "pole 'fields' instance ".$instId.":\n"; print_r($fields); echo "\n";        
    }
    // operace po zpracování dat ve všech tabulkách všech instancí

    // diagnostická tabulka - výstup pole $statuses
    $out_arrStat = new \Keboola\Csv\CsvFile($dataDir."out".$ds."tables".$ds."out_arrStat.csv");
    $out_arrStat -> writeRow(["id_status_internal", "title", "id_statuses_orig"]);
    foreach ($statuses as $statId => $statVals) {
        $colStatusesVals = [$statId, $statVals["title"], json_encode($statVals["statusIdOrig"])];
        $out_arrStat -> writeRow($colStatusesVals);
    }
    
    $idFormatIdEnoughDigits = true;         // potvrzení, že počet číslic určený proměnnou $idFormat["idTab"] dostačoval k indexaci záznamů u všech tabulek (vč. out-only položek)
}
// ==============================================================================================================================================================================================
// [B] tabulky společné pro všechny instance (nesestavené ze záznamů více instancí)
// instances
foreach ($instances as $instId => $inst) {
    $out_instances -> writeRow([$instId, $inst["url"]]);
}
logInfo("TRANSFORMACE DOKONČENA");          // volitelný diagnostický výstup do logu